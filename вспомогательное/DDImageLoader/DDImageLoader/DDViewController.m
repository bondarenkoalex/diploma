//
//  DDViewController.m
//  DDImageLoader
//
//  Created by Bondarenko Alexander on 1/14/14.
//  Copyright (c) 2014 DeadDog. All rights reserved.
//

#import "DDViewController.h"
#import "DDImageListView.h"
#import "DDImageLoader.h"
#import "DDRequestor.h"

@interface DDViewController ()

@property (nonatomic, strong) DDImageListView *imageListView;
@property (nonatomic, strong) DDImageLoader *imageLoader;


@end

@implementation DDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	self.imageListView = [[DDImageListView alloc] init];
    self.imageListView.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.imageListView];
    
    self.imageLoader = [[DDImageLoader alloc] init];

    [[DDRequestor requestor] loadImagesURLWithBlock:^(NSArray *imageNames) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.imageLoader loadImagesWithNames:imageNames withComplitionBlock:^(UIImage *image) {
                [self.imageListView performSelectorOnMainThread:@selector(addImage:) withObject:image waitUntilDone:YES];
            }];
        });
    }];
}



@end
