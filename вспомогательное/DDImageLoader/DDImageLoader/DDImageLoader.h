//
//  DDImageLoader.h
//  DDImageLoader
//
//  Created by Bondarenko Alexander on 1/14/14.
//  Copyright (c) 2014 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDImageLoader : NSObject

- (void)loadImagesWithNames:(NSArray *)names withComplitionBlock:(void(^)(UIImage *image))block;

@end
