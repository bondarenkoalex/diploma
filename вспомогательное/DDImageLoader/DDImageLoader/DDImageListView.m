//
//  DDImageListView.m
//  DDImageLoader
//
//  Created by Bondarenko Alexander on 1/14/14.
//  Copyright (c) 2014 DeadDog. All rights reserved.
//

#import "DDImageListView.h"

@interface DDImageListView()

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *imageViews;

@end


@implementation DDImageListView

- (id)init
{
    self = [super init];
    
    if (self){
        self.imageViews = [[NSMutableArray alloc] init];
        self.scrollView = [[UIScrollView alloc] init];
        self.scrollView.backgroundColor = [UIColor redColor];
        [self addSubview:self.scrollView];
    }
    
    return self;
}

#pragma mark public methods

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.scrollView.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height);
    [self resizeImageViews];
}

- (void)clear
{
    [self.imageViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.imageViews removeAllObjects];
    self.scrollView.contentSize = CGSizeZero;
}

- (void)addImage:(UIImage *)image
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, [self maxYPosition], self.frame.size.width, self.frame.size.width)];
    
    imageView.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                  UIViewAutoresizingFlexibleHeight);
    
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    imageView.backgroundColor = [UIColor blackColor];
    
    imageView.image = image;
    
    [self.scrollView addSubview:imageView];
    [self.imageViews addObject:imageView];
    
    [self resizeContentSize];
}

#pragma mark private methods

- (CGFloat)maxYPosition
{
    UIView *lastView = [self.imageViews lastObject];

    return CGRectGetMaxY(lastView.frame);
}

- (void)resizeImageViews
{
    CGPoint currentPosition = CGPointZero;
    
    for (UIImageView *imageView in self.imageViews) {
        imageView.frame = CGRectMake(currentPosition.x, currentPosition.y, self.frame.size.width, self.frame.size.width);
        currentPosition.y += self.frame.size.width;
    }
    
    [self resizeContentSize];
}

- (void)resizeContentSize
{
    self.scrollView.contentSize = CGSizeMake(self.frame.size.width, [self maxYPosition]);
}


@end
