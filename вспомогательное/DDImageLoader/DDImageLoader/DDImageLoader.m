//
//  DDImageLoader.m
//  DDImageLoader
//
//  Created by Bondarenko Alexander on 1/14/14.
//  Copyright (c) 2014 DeadDog. All rights reserved.
//

#import "DDImageLoader.h"

@interface DDImageLoader()

@property (nonatomic, strong) NSOperationQueue *operationQueue;

@end

@implementation DDImageLoader

- (id)init
{
    self = [super init];
    
    if (self){
        self.operationQueue = [[NSOperationQueue alloc] init];
        self.operationQueue.maxConcurrentOperationCount = 1;
    }
    
    return self;
}

- (void)loadImagesWithNames:(NSArray *)names withComplitionBlock:(void(^)(UIImage *image))block
{
    for (NSString *imagePath in names) {
        NSOperation *operation = [NSBlockOperation blockOperationWithBlock:^{   
            if (block){
                block([UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]]]);
            }
        }];
        [self.operationQueue addOperation:operation];
    }
}

@end
