//
//  DDAppDelegate.h
//  DDImageLoader
//
//  Created by Bondarenko Alexander on 1/14/14.
//  Copyright (c) 2014 DeadDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
