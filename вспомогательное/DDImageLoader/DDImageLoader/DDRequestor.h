//
//  DDRequestor.h
//  DDImageLoader
//
//  Created by Bondarenko Alexander on 1/14/14.
//  Copyright (c) 2014 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDRequestor : NSObject

+ (DDRequestor *)requestor;
- (void)loadImagesURLWithBlock:(void(^)(NSArray *imageNames))block;


@end
