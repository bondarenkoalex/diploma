//
//  DDRequestor.m
//  DDImageLoader
//
//  Created by Bondarenko Alexander on 1/14/14.
//  Copyright (c) 2014 DeadDog. All rights reserved.
//

#import "DDRequestor.h"
#import "AFNetworking.h"

static NSString * const kSUServerURL = @"";
static NSString * const kSUImageListAPI = @"/kSUImageListAPI";

static DDRequestor *requestor = nil;

@interface DDRequestor()

@property (nonatomic, strong) AFHTTPClient *HTTPClient;

@end


@implementation DDRequestor

+ (DDRequestor *)requestor
{
    return requestor;
}

+ (void)initialize
{
    requestor = [[DDRequestor alloc] init];
}

- (id)init
{
    self = [super init];
    
    if (self){
        self.HTTPClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:kSUServerURL]];
    }
    
    return self;
}

- (void)loadImagesURLWithBlock:(void(^)(NSArray *imageNames))block
{
#warning TODO: Uncomment when the backend finished
    
//    [self.HTTPClient getPath:kSUImageListAPI parameters:@{} success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if (block){
//           block([NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil]);
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        if (block){
//            block(nil);
//        }
//    }];
    
    NSArray *imageNames = @[@"http://os1.i.ua/3/1/328257_6768f10a.jpg",
                            @"http://animalworld.com.ua/images/2011/August/Animals/Felis-margarita/Felis-margarita_5.jpg",
                            @"http://funcats.by/uploads/2011/08/felis_margarita_01.jpg",
                            @"http://cdn.bolshoyvopros.ru/files/users/images/c2/ac/c2ac9c4ee41fb54754a847b123433f64.jpg",
                            @"http://img1.1tv.ru/imgsize640x360/PR20121231152131.JPG",
                            @"http://img0.liveinternet.ru/images/attach/c/3/75/897/75897034_3344011_Rijii_kot.jpg",
                            @"http://os1.i.ua/3/1/9851386_912bae2.jpg"];
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (block){
            block(imageNames);
        }
    });
}


@end
