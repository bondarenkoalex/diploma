//
//  DDImageListView.h
//  DDImageLoader
//
//  Created by Bondarenko Alexander on 1/14/14.
//  Copyright (c) 2014 DeadDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDImageListView : UIView

- (void)clear;
- (void)addImage:(UIImage *)image;

@end
