﻿namespace laba1
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.fromTextView = new System.Windows.Forms.TextBox();
            this.toTextView = new System.Windows.Forms.TextBox();
            this.fromComboBox = new System.Windows.Forms.ComboBox();
            this.toComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // fromTextView
            // 
            this.fromTextView.AcceptsReturn = true;
            this.fromTextView.Location = new System.Drawing.Point(21, 52);
            this.fromTextView.Name = "fromTextView";
            this.fromTextView.Size = new System.Drawing.Size(202, 21);
            this.fromTextView.TabIndex = 0;
            this.fromTextView.TextChanged += new System.EventHandler(this.fromTextView_TextChanged);
            this.fromTextView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fromTextView_KeyPressed);
            // 
            // toTextView
            // 
            this.toTextView.AcceptsReturn = true;
            this.toTextView.Location = new System.Drawing.Point(21, 235);
            this.toTextView.Name = "toTextView";
            this.toTextView.ReadOnly = true;
            this.toTextView.Size = new System.Drawing.Size(202, 21);
            this.toTextView.TabIndex = 1;
            this.toTextView.TextChanged += new System.EventHandler(this.toTextView_TextChanged);
            // 
            // fromComboBox
            // 
            this.fromComboBox.Location = new System.Drawing.Point(21, 103);
            this.fromComboBox.Name = "fromComboBox";
            this.fromComboBox.Size = new System.Drawing.Size(202, 22);
            this.fromComboBox.TabIndex = 2;
            this.fromComboBox.SelectedIndexChanged += new System.EventHandler(this.fromComboBox_SelectedIndexChanged);
            // 
            // toComboBox
            // 
            this.toComboBox.Location = new System.Drawing.Point(21, 207);
            this.toComboBox.Name = "toComboBox";
            this.toComboBox.Size = new System.Drawing.Size(202, 22);
            this.toComboBox.TabIndex = 3;
            this.toComboBox.SelectedIndexChanged += new System.EventHandler(this.toComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(21, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 20);
            this.label1.Text = "Из";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(21, 184);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 20);
            this.label2.Text = "В";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toComboBox);
            this.Controls.Add(this.fromComboBox);
            this.Controls.Add(this.toTextView);
            this.Controls.Add(this.fromTextView);
            this.Menu = this.mainMenu1;
            this.Name = "mainForm";
            this.Text = "mainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox fromTextView;
        private System.Windows.Forms.TextBox toTextView;
        private System.Windows.Forms.ComboBox fromComboBox;
        private System.Windows.Forms.ComboBox toComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

