﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace laba1
{
    public partial class mainForm : Form
    {
        public string[] titles = { "унция", "грамм", "карат" };
        public double[] values = { 1.0f, 28.353495f, 142f }; //1.	1 унция = 28.353495 г = 142 карата;

        public mainForm()
        {
            InitializeComponent();
            addComboBoxesItems();
        }

        private void addComboBoxesItems()
        {
            foreach(string title in this.titles)
            {
                fromComboBox.Items.Add(title);
                toComboBox.Items.Add(title);
            }

            fromComboBox.SelectedIndex = 0;
            toComboBox.SelectedIndex = 0;
        }

        public void recalculateResult()
        {
            try
            {
                double result = 0.0d;
                string resultString = "0.0";

                double firstValueCof = values[fromComboBox.SelectedIndex];
                double secondValueCof = values[toComboBox.SelectedIndex];

                result = (double.Parse(fromTextView.Text) / firstValueCof) * secondValueCof;

                resultString = result.ToString();
                toTextView.Text = resultString;
            }
            catch (Exception)
            {

            }
        }

        private void fromTextView_TextChanged(object sender, EventArgs e)
        {
            recalculateResult();
        }

        private void fromTextView_KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void fromComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            recalculateResult();
        }

        private void toComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            recalculateResult();
        }

        private void toTextView_TextChanged(object sender, EventArgs e)
        {

        }
    }
}