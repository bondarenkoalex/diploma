﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace laba2
{
    public class QuestionCollection
    {
        public List<question> Questions;
    }

    public class answer
    {
        public string anserTitle;
        public Boolean isCorrect;
    }

    public class question
    {
        public question()
        {
            answers = new List<answer>();
        }

        public string questionTitle;
        public List<answer> answers;
    }
}
