﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Xml.Serialization;

namespace laba2
{
    public class Loader
    {
        public Loader()
        {

        }

        public List<question> loadQestions()
        {
            var questions = new List<question>();

            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Файлы XML (*.xml)|*.xml";
            if (dlg.ShowDialog() != DialogResult.OK)
            {
                return new List<question>();
            }

            XmlTextReader reader = null;

            var deserializer = new XmlSerializer(typeof(QuestionCollection));

            var stream = new StreamReader(dlg.FileName);

            var q = (QuestionCollection)deserializer.Deserialize(stream);

            return q.Questions;
        }
    }
}
