﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace laba2
{
    public partial class mainForm : Form
    {
        public List<question> questions;
        public Loader loader;
        public int curretQuestionIndex;

        public mainForm()
        {
            InitializeComponent();
            curretQuestionIndex = 0;
            loader = new Loader();
            questions = loader.loadQestions();
            reload();
        }

        public void reload()
        {
            if (curretQuestionIndex >= questions.Count)
            {
                return;
            }

            questionlabel.ForeColor = System.Drawing.Color.Black;
            nextButton.Visible = false;
            answerComboBox.Enabled = true;

            question question = questions[curretQuestionIndex];
            reloadWithQuestion(question);
        }

        public void reloadWithQuestion(question question)
        {
            answerComboBox.Items.Clear();
            foreach (answer item in question.answers)
            {
                answerComboBox.Items.Add(item.anserTitle);
            }
            questionlabel.Text = question.questionTitle;
        }

        public void setSelectedAnswerIndex(int index)
        {
            question question = questions[curretQuestionIndex];

            if (question.answers[index].isCorrect == true)
            {
                questionlabel.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                questionlabel.ForeColor = System.Drawing.Color.Red;
            }

            answerComboBox.Enabled = false;
            nextButton.Visible = true;
        }

        private void answerComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            setSelectedAnswerIndex(answerComboBox.SelectedIndex);
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            curretQuestionIndex++;
            reload();
        }
    }
}