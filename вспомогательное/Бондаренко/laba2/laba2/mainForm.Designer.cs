﻿namespace laba2
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.questionlabel = new System.Windows.Forms.Label();
            this.answerComboBox = new System.Windows.Forms.ComboBox();
            this.nextButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // questionlabel
            // 
            this.questionlabel.Location = new System.Drawing.Point(17, 28);
            this.questionlabel.Name = "questionlabel";
            this.questionlabel.Size = new System.Drawing.Size(210, 20);
            this.questionlabel.Text = "questionlabel";
            this.questionlabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // answerComboBox
            // 
            this.answerComboBox.Location = new System.Drawing.Point(17, 75);
            this.answerComboBox.Name = "answerComboBox";
            this.answerComboBox.Size = new System.Drawing.Size(210, 22);
            this.answerComboBox.TabIndex = 1;
            this.answerComboBox.SelectedIndexChanged += new System.EventHandler(this.answerComboBox_SelectedIndexChanged);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(17, 229);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(210, 20);
            this.nextButton.TabIndex = 2;
            this.nextButton.Text = "next";
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.answerComboBox);
            this.Controls.Add(this.questionlabel);
            this.Menu = this.mainMenu1;
            this.Name = "mainForm";
            this.Text = "mainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label questionlabel;
        private System.Windows.Forms.ComboBox answerComboBox;
        private System.Windows.Forms.Button nextButton;
    }
}