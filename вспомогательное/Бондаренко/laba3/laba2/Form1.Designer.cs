﻿namespace laba2
{
    partial class startForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.nextButton = new System.Windows.Forms.Button();
            this.countTextView = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(65, 122);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(121, 20);
            this.nextButton.TabIndex = 0;
            this.nextButton.Text = "go";
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // countTextView
            // 
            this.countTextView.Location = new System.Drawing.Point(65, 50);
            this.countTextView.Name = "countTextView";
            this.countTextView.Size = new System.Drawing.Size(121, 21);
            this.countTextView.TabIndex = 1;
            this.countTextView.TextChanged += new System.EventHandler(this.countTextView_TextChanged);
            this.countTextView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.countTextView_KeyPressed);
            // 
            // startForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.countTextView);
            this.Controls.Add(this.nextButton);
            this.Menu = this.mainMenu1;
            this.Name = "startForm";
            this.Text = "Go-go";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.TextBox countTextView;
    }
}

