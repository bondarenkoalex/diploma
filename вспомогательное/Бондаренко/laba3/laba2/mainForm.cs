﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace laba2
{
    public partial class mainForm : Form
    {
        public int maxCount;
        public List<question> questions;
        public Loader loader;
        public int curretQuestionIndex;
        public List<int> oldIndexes;

        public int bedCount;
        public int okCount;

        public mainForm(int newMaxCount)
        {
            InitializeComponent();
            curretQuestionIndex = 0;
            loader = new Loader();
            oldIndexes = new List<int>();
            questions = loader.loadQestions();

            bedCount = 0;
            okCount = 0;

            maxCount = newMaxCount;

            if (questions.Count < maxCount)
            {
                maxCount = questions.Count;
            }

            reload();
        }

        public double percentOkAnswers()
        {
            double result = 0.0;

            result = okCount / ((double)(okCount + bedCount) / 100.0);

            return result;
        }

        public void reload()
        {
            if (curretQuestionIndex >= maxCount)
            {

                congrForm form = new congrForm(percentOkAnswers());
                form.Show();

                return;
            }

            questionlabel.ForeColor = System.Drawing.Color.Black;
            nextButton.Visible = false;
            answerComboBox.Enabled = true;
            question question = questions[randIndex()];
            reloadWithQuestion(question);
        }

        public int randIndex()
        {
            int radonInt = (int)(new Random().Next(0,questions.Count - 1));

            while(true)
            {
                if (oldIndexes.Exists(index => index == radonInt))
                {
                    radonInt = new Random().Next(0, questions.Count);
                }
                else
                {
                    break;
                }
            }

            oldIndexes.Add(radonInt);

            return radonInt;
        }

        public void reloadWithQuestion(question question)
        {
            answerComboBox.Items.Clear();
            foreach (answer item in question.answers)
            {
                answerComboBox.Items.Add(item.anserTitle);
            }
            questionlabel.Text = question.questionTitle;
        }

        public void setSelectedAnswerIndex(int index)
        {
            question question = questions[curretQuestionIndex];

            if (question.answers[index].isCorrect == true)
            {
                questionlabel.ForeColor = System.Drawing.Color.Green;
                okCount++;
            }
            else
            {
                questionlabel.ForeColor = System.Drawing.Color.Red;
                bedCount++;
            }

            answerComboBox.Enabled = false;
            nextButton.Visible = true;
        }

        private void answerComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            setSelectedAnswerIndex(answerComboBox.SelectedIndex);
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            curretQuestionIndex++;
            reload();
        }
    }
}