﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace laba2
{
    public partial class startForm : Form
    {
        public int count;

        public startForm()
        {
            InitializeComponent();
            count = 0;
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            mainForm form = new mainForm(count);
            form.Show();
        }

        private void countTextView_KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void countTextView_TextChanged(object sender, EventArgs e)
        {
            count = int.Parse(countTextView.Text);
        }
    }
}