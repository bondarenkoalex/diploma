//
//  DDMailDataObject.m
//  testDataManager
//
//  Created by Bondarenko Alexander on 1/17/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDMailDataObject.h"
#import "DDMailData.h"
#import "DDFileManager.h"

@interface DDMailDataObject()

- (float)compressionValueWithPhotoQualityType:(DDPhotoQualityType)type;

- (NSData *)dataWithData:(NSData *)data withCompression:(float)compression;
- (NSData *)dataWithData:(NSData *)data withCompression:(float *)compression withResultCompression:(float)resultCompression;

@end

@implementation DDMailDataObject

@synthesize comment;
@synthesize locationTag;
@synthesize orientation;
@synthesize photoQuality;
@synthesize photoSize;
@synthesize sendingTime;
@synthesize username;
@synthesize thumbnail;
@synthesize date;
@synthesize photoObject;
@synthesize numberOfCopies;

- (id)init
{
    self = [super initWithName:[DDMailDataObject dataObjectName]];
    if (self)
    {
        
    }
    return self;
}

- (void)fillSelfWithCoreDataObject:(NSManagedObject *)coreDataObject
{
    DDMailData *mailData = (DDMailData *)coreDataObject;
    
    self.comment = [mailData.comment copy];
    self.locationTag = [mailData.locationTag copy];
    self.orientation = [mailData.orientation intValue];
    self.photoQuality = [mailData.photoQuality intValue];
    self.photoSize = [mailData.photoSize intValue];
    self.sendingTime = [mailData.sendingTime  copy];
    self.username = [mailData.username copy];
    self.thumbnail = [UIImage imageWithData:mailData.thumbnail];
    self.date = [mailData.date copy];
    self.numberOfCopies = [mailData.copyCount intValue];
    self.status = [mailData.status intValue];
    
    if (mailData.photo)
    {
        self.photoObject = [[DDPhotoFileObject alloc] init];
        [self.photoObject fillSelfWithCoreDataObject:((NSManagedObject *)mailData.photo)];
    }
}

- (void)fillCoreDataObjectWithCoreDataObject:(NSManagedObject *)coreDataObject
                              withRootObject:(NSManagedObject *)rootCoreDataObject
                                   withBlock:(void(^)(NSArray *arrayOfDataObjects, NSManagedObject *rootCoreDataObject))block
{
    DDMailData *mailData = (DDMailData *)coreDataObject;

    mailData.comment = [self.comment copy];
    mailData.locationTag = [self.locationTag copy];
    mailData.orientation = [NSNumber numberWithInt:self.orientation];
    mailData.photoQuality = [NSNumber numberWithInt:self.photoQuality];
    mailData.photoSize = [NSNumber numberWithInt:self.photoSize];
    mailData.sendingTime = [self.sendingTime  copy];
    mailData.username = [self.username copy];
    mailData.thumbnail = UIImagePNGRepresentation(self.thumbnail);
    mailData.date = [self.date copy];
    mailData.copyCount = [NSNumber numberWithInt:self.numberOfCopies];
    mailData.status = [NSNumber numberWithInt:self.status];

    NSMutableArray *dataObjects = [[NSMutableArray alloc] init];

    if (self.photoObject)
    {
        [dataObjects addObject:self.photoObject];
    }

    block(dataObjects,mailData);
}

- (BOOL)isEqualToCoreDataObject:(NSManagedObject *)coreDataObject
{
    return [((DDMailData*)coreDataObject).date isEqualToDate:self.date];
}

+ (NSString *)dataObjectName
{
    return kDDMailDataObjectName;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"date = %@,username =%@, photoFileObject = %@",date,username,photoObject];
}

- (BOOL)canRemoveWithoutPropertiesSaving
{
    return [DDFileManager removePhotoDataWithPhotoFileObject:self.photoObject];
}

- (float)compressionValueWithPhotoQualityType:(DDPhotoQualityType)type
{
    float compression = 0.0;
    
    switch (self.photoQuality)
	{
		case DDPhotoQualityTypeActual:
		{
            compression = kDDActualQuality;
            break;
		}
            
		case DDPhotoQualityTypeBig:
		{
            compression = kDDBigQuality;
            break;
		}
            
		case DDPhotoQualityTypeMedium:
		{
            compression = kDDMediumQuality;
            break;
		}
            
		case DDPhotoQualityTypeSmall:
		{
            compression = kDDSmallQuality;
            break;
		}
	}
    
    return compression;
}

- (NSData *)dataWithData:(NSData *)data withCompression:(float *)compression withResultCompression:(float)resultCompression
{
    NSData *result = UIImageJPEGRepresentation([UIImage imageWithData:data], resultCompression);
    *compression = MAX_PHOTO_MEMORY_SIZE_IN_BYTES * 1.0 / [result length];
    
    return result;
}

- (NSData *)dataWithData:(NSData *)data withCompression:(float)compression
{
    float owerfow = (float)[data length] / MAX_PHOTO_MEMORY_SIZE_IN_BYTES;
    float resultCompression = compression / MAX(owerfow, 1.0);
    
    NSData *result = [self dataWithData:data withCompression:&compression withResultCompression:resultCompression];
    
    while (compression < 1.0)
    {
        compression *= kDDCompressionValue;
        result = [self dataWithData:result withCompression:&compression withResultCompression:compression];
    }
    
    return result;
}

- (NSData *)photoDataWithCurrentQuality
{
    NSData *imageData = [DDFileManager loadPhotoDataWithPhotoFileObject:self.photoObject];
    float compression = [self compressionValueWithPhotoQualityType:self.photoQuality];
    NSData *result = [self dataWithData:imageData withCompression:compression];
    
	return result;
}

@end
