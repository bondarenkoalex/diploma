//
//  DDMailDataObject.h
//  testDataManager
//
//  Created by Bondarenko Alexander on 1/17/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDDataObject.h"
#import "DDPhotoFileObject.h"
#import "DDFileManager.h"

static NSString * const kDDMailDataObjectName = @"DDMailData";
static float const kDDActualQuality = 1.0f;
static float const kDDBigQuality = 0.8f;
static float const kDDMediumQuality = 0.5f;
static float const kDDSmallQuality = 0.3f;
static float const kDDCompressionValue = 0.9f;

#define MAX_PHOTO_MEMORY_SIZE_IN_BYTES ( 4 * 1024 * 1024 )

typedef enum DDPhotoQuality
{
    DDPhotoQualityTypeSmall = 0,
    DDPhotoQualityTypeMedium = 1,
    DDPhotoQualityTypeBig = 2,
	DDPhotoQualityTypeActual = 3
} DDPhotoQualityType;

typedef enum
{
    DDMailDataObjectSendingStatusSending = 0,
    DDMailDataObjectSendingStatusInQueue = 1,
    DDMailDataObjectSendingStatusSuccessful = 2,
    DDMailDataObjectSendingStatusFail = 3,
}DDMailDataObjectSendingStatus;

typedef enum DDPreviewPhotoOrientation
{
    DDPreviewSquareOrientation = 0,
    DDPreviewHorizontalOrientation = 1,
    DDPreviewVerticatlOrientation = 2
} DDPreviewPhotoOrientation;

/**
	Class entity of the mail
 */
@interface DDMailDataObject : DDDataObject

@property (nonatomic, readwrite) DDMailDataObjectSendingStatus status;
@property (nonatomic, strong) NSString *comment;
@property (nonatomic, readwrite) int numberOfCopies;
@property (nonatomic, strong) NSString *locationTag;
@property (nonatomic, readwrite) DDPreviewPhotoOrientation orientation;
@property (nonatomic, readwrite) DDPhotoQualityType photoQuality;
@property (nonatomic, readwrite) int photoSize;
@property (nonatomic, strong) NSDate *sendingTime;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) DDPhotoFileObject *photoObject;

- (NSData *)photoDataWithCurrentQuality;

@end
