//
//  DDDataObject.m
//  testDataManager
//
//  Created by Bondarenko Alexander on 1/17/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDDataObject.h"

@implementation DDDataObject
@synthesize coreDataTypeName;

- (id)initWithName:(NSString*)newCoreDataTypeName
{
    self = [super init];
    if (self)
    {
        coreDataTypeName = newCoreDataTypeName;
    }
    return self;
}

- (void)fillSelfWithCoreDataObject:(NSManagedObject*)coreDataObject
{
    NSAssert(NO, @"not override method fillSelfWithCoreDataObject");  
}

- (void)fillCoreDataObjectWithCoreDataObject:(NSManagedObject*)coreDataObject
                              withRootObject:(NSManagedObject*)rootCoreDataObject
                                   withBlock:(void(^)(NSArray* arrayOfDataObjects,NSManagedObject *rootCoreDataObject))block
{
    NSAssert(NO, @"not override method fillCoreDataObjectWithCoreDataObject");
}

- (BOOL)isEqualToCoreDataObject:(NSManagedObject*)coreDataObject
{
    NSAssert(NO, @"not override method isEqualToCoreDataObject");
    return NO;
}

+ (NSString*)dataObjectName
{
    NSAssert(NO, @"not override method dataObjectName");
    return nil;
}

- (BOOL)canRemoveWithoutPropertiesSaving
{
      NSAssert(NO, @"not override method remove");
      return NO;
}

@end
