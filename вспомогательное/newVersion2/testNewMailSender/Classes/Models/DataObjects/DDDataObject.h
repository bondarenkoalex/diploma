//
//  DDDataObject.h
//  testDataManager
//
//  Created by Bondarenko Alexander on 1/17/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


/**
	Base class for no CoreData model objects
 */
@interface DDDataObject : NSObject

/**
	CoreData entity name for this objec
 */
@property(nonatomic,readonly) NSString *coreDataTypeName;

/**
	Function to populate the fields of the coreDataObject from this class
	@param coreDataObject CoreData object corresponding to this object
	@param rootCoreDataObject previous CoreData object in the chain filling
    @param block block to which one must pass an array of CoreData objects for further filling and coreDataObject 
 */
- (void)fillCoreDataObjectWithCoreDataObject:(NSManagedObject*)coreDataObject
                              withRootObject:(NSManagedObject*)rootCoreDataObject
                                   withBlock:(void(^)(NSArray *arrayOfDataObjects,NSManagedObject *rootCoreDataObject))block;

/**
	Compare function of this object with the object of CoreData
	@param coreDataObject CoreData object to compare
	@returns returns YES if the objects are equivalent
 */
- (BOOL)isEqualToCoreDataObject:(NSManagedObject*)coreDataObject;

/**
	Initializes self with the name of the CoreData entity
	@param coreDataTypeName CoreData entity name for this class
	@returns return self
 */
- (id)initWithName:(NSString*)coreDataTypeName;

/**
	Fill self from CoreData object
	@param CoreData object of which filled their fields 
 */
- (void)fillSelfWithCoreDataObject:(NSManagedObject*)coreDataObject;

/**
	Prepare self for removal
	@returns return NO if need re-save this object
 */
- (BOOL)canRemoveWithoutPropertiesSaving;

/**
	Return the string with the name of the entity
	@returns CoreData entity name for this class
 */
+ (NSString*)dataObjectName;

@end
