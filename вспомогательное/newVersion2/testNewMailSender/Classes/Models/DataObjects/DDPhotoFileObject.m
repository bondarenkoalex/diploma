//
//  DDPhotoFileObject.m
//  testDataManager
//
//  Created by Bondarenko Alexander on 1/17/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDPhotoFileObject.h"
#import "DDMailDataObject.h"
#import "DDPhotoFile.h"

@implementation DDPhotoFileObject

@synthesize fileName;
@synthesize linksCount;

- (id)init
{
    self = [super initWithName:[DDPhotoFileObject dataObjectName]];
    if (self)
    {
        
    }
    return self;
}

- (void)fillSelfWithCoreDataObject:(NSManagedObject *)coreDataObject
{
    DDPhotoFile *photoData = (DDPhotoFile *)coreDataObject;
    self.linksCount = [photoData.linksCount intValue];
    self.fileName = [photoData.fileName copy];
}

- (void)fillCoreDataObjectWithCoreDataObject:(NSManagedObject *)coreDataObject
                              withRootObject:(NSManagedObject *)rootCoreDataObject
                                   withBlock:(void(^)(NSArray *arrayOfDataObjects,NSManagedObject *rootCoreDataObject))block
{
    DDPhotoFile *photoData = (DDPhotoFile *)coreDataObject;

    photoData.linksCount = [NSNumber numberWithInt:self.linksCount];
    photoData.fileName = [self.fileName copy];
    [photoData addDataObject:(DDMailData *)rootCoreDataObject];;
    
    block(nil,photoData);
}

- (BOOL)isEqualToCoreDataObject:(NSManagedObject *)coreDataObject
{
    return [((DDPhotoFile*)coreDataObject).fileName isEqualToString:self.fileName];
}

+ (NSString *)dataObjectName
{
    return kDDPhotoDataObjectName;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"lincCount = %d,imageName =%@",linksCount,fileName];
}

- (BOOL)canRemoveWithoutPropertiesSaving
{
    return YES;
}

@end
