//
//  DDPhotoFileObject.h
//  testDataManager
//
//  Created by Bondarenko Alexander on 1/17/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDDataObject.h"

static NSString * const kDDPhotoDataObjectName = @"DDPhotoFile";

/**
 Class entity of the photo
 */
@interface DDPhotoFileObject : DDDataObject

@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, readwrite) int linksCount;

@end
