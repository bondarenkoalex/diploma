//
//  PhotoFile.m
//  mailSender
//
//  Created by Bondarenko Alexander on 12/14/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDPhotoFile.h"
#import "DDMailData.h"


@implementation DDPhotoFile

@dynamic fileName;
@dynamic linksCount;
@dynamic data;

@end
