//
//  DDMailData.h
//  mailSender
//
//  Created by Alexander Bondarenko on 03.01.13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DDPhotoFile;

@interface DDMailData : NSManagedObject

@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSNumber * copyCount;
@property (nonatomic, retain) NSString * locationTag;
@property (nonatomic, retain) NSNumber * orientation;
@property (nonatomic, retain) NSNumber * photoQuality;
@property (nonatomic, retain) NSNumber * photoSize;
@property (nonatomic, retain) NSDate * sendingTime;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSData * thumbnail;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) DDPhotoFile *photo;
@property (nonatomic, retain) NSNumber * status;

@end

