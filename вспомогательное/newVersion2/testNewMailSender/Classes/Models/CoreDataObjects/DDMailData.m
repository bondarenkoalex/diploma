//
//  DDMailData.m
//  mailSender
//
//  Created by Alexander Bondarenko on 03.01.13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDMailData.h"
#import "DDPhotoFile.h"
#import "DDFileManager.h"
#import "DDDatabaseMananger.h"

@implementation DDMailData

@dynamic comment;
@dynamic copyCount;
@dynamic locationTag;
@dynamic orientation;
@dynamic photoQuality;
@dynamic photoSize;
@dynamic sendingTime;
@dynamic username;
@dynamic thumbnail;
@dynamic photo;
@dynamic date;
@dynamic status;

@end
