//
//  PhotoFile.h
//  mailSender
//
//  Created by Bondarenko Alexander on 12/14/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DDMailData;

@interface DDPhotoFile : NSManagedObject

@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSNumber * linksCount;
@property (nonatomic, retain) NSSet *data;

@end

@interface DDPhotoFile (CoreDataGeneratedAccessors)

- (void)addDataObject:(DDMailData *)value;
- (void)removeDataObject:(DDMailData *)value;
- (void)addData:(NSSet *)values;
- (void)removeData:(NSSet *)values;

@end
