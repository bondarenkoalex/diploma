//
//  DDAppDelegate.h
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 2/6/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kDDApplicationPauseNotificationName = @"pause";
static NSString * const kDDApplicationResumeNotificationName = @"resume";

@interface DDAppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end
