//
//  DDTopViewInfo.h
//  testNot
//
//  Created by Bondarenko Alexander on 12/19/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>

static float const kDDTopViewInfoAnimationDuration = 0.5f;
static float const kDDTopViewInfoWaitingAfterAnimationDuration = 1.5f;
static float const kDDTopViewInfoWaitingBeforeAnimationDuration = 1.0f;
static NSString * const kDDTopViewInfoNibName = @"DDTopInfoView";

@interface DDTopViewInfoDescription : NSObject

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *message;

@end

@interface DDTopViewInfo : NSObject

+ (DDTopViewInfo *)defaultTopViewInfo;
- (void)showMessageWithDescription:(DDTopViewInfoDescription *)message;
- (void)hide;

@end
