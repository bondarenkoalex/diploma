//
//  DDInfoView.h
//  testNot
//
//  Created by Bondarenko Alexander on 12/19/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDInfoView : UIView

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UIView *bgView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end
