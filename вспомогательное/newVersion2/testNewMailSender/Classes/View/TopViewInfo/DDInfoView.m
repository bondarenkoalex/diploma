//
//  DDInfoView.m
//  testNot
//
//  Created by Bondarenko Alexander on 12/19/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDInfoView.h"
#import <QuartzCore/QuartzCore.h>

@implementation DDInfoView

- (void)awakeFromNib
{
    self.bgView.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    self.bgView.layer.cornerRadius = 8;
    self.bgView.layer.borderWidth = 0.4;
    self.bgView.layer.masksToBounds = YES;
    self.imageView.layer.cornerRadius = 4;
    self.imageView.layer.masksToBounds = YES;
}

@end
