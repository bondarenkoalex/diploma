//
//  DDTopViewInfo.m
//  testNot
//
//  Created by Bondarenko Alexander on 12/19/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDTopViewInfo.h"
#import "DDInfoView.h"
#import "DDAppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation DDTopViewInfoDescription

@synthesize title;
@synthesize message;

@end

static DDTopViewInfo *topViewInfo = nil;

@interface DDTopViewInfo()

@property(nonatomic,strong) UIWindow *statusWindow;
@property(nonatomic,weak) UIWindow *window;
@property(nonatomic,strong) DDInfoView *view;
@property(nonatomic,readwrite) BOOL isShow;
@property(nonatomic,strong) NSTimer *rootTimer;

- (DDInfoView *)loadViewWithName:(NSString *)name;
- (void)hideInfoView;
- (void)showInfoView;
- (void)addTapGestureRecognizer;
- (void)addStatusWindow;
- (void)addInfoView;
- (void)tick;
- (void)stopTimer;
- (void)startTimer;
- (void)reloadTitlesWithDescription:(DDTopViewInfoDescription *)description;

@end

@implementation DDTopViewInfo

@synthesize window;
@synthesize view;
@synthesize isShow;
@synthesize rootTimer;
@synthesize statusWindow;


+ (DDTopViewInfo *)defaultTopViewInfo
{
    return topViewInfo;
}

+ (void)initialize
{
    topViewInfo = [[DDTopViewInfo alloc] init];
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.window = [[UIApplication sharedApplication].delegate window];
        
        [self addInfoView];
        isShow = YES;
        [self hideInfoView];
        [self addTapGestureRecognizer];
    }
    return self;
}

- (void)addInfoView
{
    self.view = [self loadViewWithName:kDDTopViewInfoNibName];
    view.userInteractionEnabled = YES;
    view.multipleTouchEnabled = YES;
    
    [self addStatusWindow];
    [window makeKeyWindow];
    [statusWindow addSubview:view];
}

- (void)addStatusWindow
{
    self.statusWindow = [[UIWindow alloc] initWithFrame:view.frame];
    statusWindow.windowLevel = UIWindowLevelStatusBar;
    statusWindow.hidden = NO;
    statusWindow.backgroundColor = [UIColor clearColor];
    [statusWindow makeKeyAndVisible];
}

- (void)addTapGestureRecognizer
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(tap:)];
    [view addGestureRecognizer:tapGesture];
}

- (void)stopTimer
{
    [rootTimer invalidate];
    rootTimer = nil;
}

- (void)startTimer
{
    self.rootTimer = [NSTimer scheduledTimerWithTimeInterval:kDDTopViewInfoWaitingAfterAnimationDuration
                                                      target:self
                                                    selector:@selector(tick)
                                                    userInfo:nil
                                                     repeats:NO];
}

- (void)reloadTitlesWithDescription:(DDTopViewInfoDescription *)description
{
    view.titleLabel.text = description.title;
    view.messageLabel.text = description.message;
}

- (void)tap:(UIGestureRecognizer *)recognizer
{
    [self hide];
}

- (DDInfoView *)loadViewWithName:(NSString *)name
{
    NSArray *previews = [[NSBundle mainBundle] loadNibNamed:name owner:self options:nil];
    DDInfoView *tempView = [previews objectAtIndex:0];
    
    return tempView;
}

- (void)hideInfoView
{
    if (!isShow)
    {
        return;
    }
    isShow = NO;
    
    CGRect tempRect = statusWindow.frame;
    tempRect.origin.x = 0.0;
    tempRect.origin.y = -tempRect.size.height;
    [statusWindow setFrame:tempRect];
}

- (void)showInfoView
{
    if (isShow)
    {
        return;
    }
    isShow = YES;
    
    CGRect tempRect = statusWindow.frame;
    tempRect.origin.x = 0.0;
    tempRect.origin.y = 0.0;
    [statusWindow setFrame:tempRect];
}

- (void)showMessageWithDescription:(DDTopViewInfoDescription *)description
{
    [self stopTimer];
    
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    
    if (isShow)
    {
        [UIView animateWithDuration:kDDTopViewInfoAnimationDuration animations:^{
            [self hideInfoView];
        } completion:^(BOOL finished) {
            [self reloadTitlesWithDescription:description];
            [UIView animateWithDuration:kDDTopViewInfoAnimationDuration animations:^{
                [self showInfoView];
            } completion:^(BOOL finished) {
                [self startTimer];
            }];
        }];
    }
    else
    {
        [self reloadTitlesWithDescription:description];
        
        [UIView animateWithDuration:kDDTopViewInfoAnimationDuration
                              delay:kDDTopViewInfoWaitingBeforeAnimationDuration
                            options:UIViewAnimationOptionTransitionNone animations:^{
            [self showInfoView];    
        } completion:^(BOOL finished) {
            [self startTimer];   
        }];
    } 
}
- (void)hide
{
    [self stopTimer];
    
    [UIView animateWithDuration:kDDTopViewInfoAnimationDuration animations:^{
        [self hideInfoView];
    } completion:nil];
}

- (void)tick
{
    [self hide];
}

@end
