//
//  DDMessage.h
//  mailSender
//
//  Created by Bondarenko Alexander on 1/11/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "SKPSMTPMessage.h"
#import "DDMailDataObject.h"

static NSString * const kDDMessagePass = @"ddMailSender1990";
static NSString * const kDDMessageLogin = @"ddMailSender";
static NSString * const kDDMessageHost = @"smtp.gmail.com";
static NSString * const kDDMessageRecipientAddress = @"bondarenkoaleksandr1990@gmail.com";
static BOOL const kDDMessageWantsSecure = YES;
static BOOL const kDDMessageUseAuthentification = YES;

@interface DDMessage : SKPSMTPMessage

- (id)initWithMailData:(DDMailDataObject *)mailData;
- (void)sendMessage;
- (void)stop;

@end
