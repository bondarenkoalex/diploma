//
//  DDMessage.m
//  mailSender
//
//  Created by Bondarenko Alexander on 1/11/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDMessage.h"
#import "NSData+Base64Additions.h"

@interface DDMessage()

@property(nonatomic,readwrite) BOOL isRun;
@property(nonatomic, strong) __block NSRunLoop *currentRunLoop;
@property(nonatomic,readwrite) dispatch_queue_t startQueue;

- (void)fillMassageWithData:(DDMailDataObject *)mailData;
- (void)stopWithRunLoop:(NSRunLoop *)currentRunLoop;

@end

@implementation DDMessage

@synthesize startQueue;
@synthesize currentRunLoop;
@synthesize isRun;

- (id)initWithMailData:(DDMailDataObject *)mailData
{
    self = [super init];
    if (self)
    {
        isRun = NO;
        [self fillMassageWithData:(DDMailDataObject *)mailData];
    }
    return self;
}

- (void)fillMassageWithData:(DDMailDataObject *)mailData
{
    self.fromEmail = [NSString stringWithFormat:@"%@@mailSender.com",kDDMessageLogin];
    self.toEmail = kDDMessageRecipientAddress;
    self.requiresAuth = kDDMessageUseAuthentification;
    self.relayHost = kDDMessageHost;
    self.login = kDDMessageLogin;
    self.pass = kDDMessagePass;
    self.wantsSecure = kDDMessageWantsSecure;
    
    NSString *subject = [NSString stringWithFormat:@"#%@, #name = %@",mailData.locationTag,mailData.username];

    subject = [NSString stringWithFormat:@"%@ %@",subject, mailData.comment];
    self.subject = subject;
    
    __block NSMutableArray *parts_to_send = [NSMutableArray array];
    
    NSData *image_data = [mailData photoDataWithCurrentQuality];
    NSDictionary *image_part = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"inline;\r\n\tfilename=\"Photo.jpeg\"",kSKPSMTPPartContentDispositionKey,
                                @"base64",kSKPSMTPPartContentTransferEncodingKey,
                                @"image/png;\r\n\tname=Success.png;\r\n\tx-unix-mode=0666",kSKPSMTPPartContentTypeKey,
                                [image_data encodeWrappedBase64ForData],kSKPSMTPPartMessageKey,
                                nil];
    
    [parts_to_send addObject:image_part];
    self.parts = parts_to_send;
}

- (void)stopWithRunLoop:(NSRunLoop *)aCurrentRunLoop
{
    [self performSelector:@selector(stopWatchdog)];
    
    [self.inputStream close];
    [self.inputStream removeFromRunLoop:aCurrentRunLoop
                           forMode:NSDefaultRunLoopMode];
    self.inputStream = nil;
    
    [self.outputStream close];
    [self.outputStream removeFromRunLoop:aCurrentRunLoop
                            forMode:NSDefaultRunLoopMode];
    self.outputStream = nil;
    
    [self.inputStream setDelegate:nil];
    [self.outputStream setDelegate:nil];
    
    self.delegate = nil;
}

- (void)sendMessage
{
    isRun = YES;

    self.startQueue = dispatch_queue_create("startQueue", nil);
    
    dispatch_async(startQueue, ^{
        [self send];
        self.currentRunLoop = [NSRunLoop currentRunLoop];

        while (isRun)
        {
            [currentRunLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    });
}

- (void)stop
{
    if (isRun)
    {
        isRun = NO;
        if (currentRunLoop)
        {
            [self stopWithRunLoop:currentRunLoop];
            self.currentRunLoop = nil;
        }
    }
}

- (void)dealloc
{
    self.startQueue = nil;
}

@end
