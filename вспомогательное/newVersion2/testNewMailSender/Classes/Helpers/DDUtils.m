//
//  DDUtils.m
//  mailSender
//
//  Created by Bondarenko Alexander on 12/5/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDUtils.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

NSString *stringFromDateWithString(NSDate *date, NSString *string)
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:string];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}
