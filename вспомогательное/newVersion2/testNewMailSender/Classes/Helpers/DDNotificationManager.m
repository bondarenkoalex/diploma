//
//  DDNotificationManager.m
//  mailSender
//
//  Created by Bondarenko Alexander on 11/29/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDNotificationManager.h"
#import "DDAppDelegate.h"
#import "DDTopViewInfo.h"
#import "DDAppDelegate.h"

@interface DDNotificationManager()

@property(nonatomic,readwrite) BOOL enable;

- (void)enableSndingNotification;
- (void)disableSndingNotification;
- (void)scheduleNotificationOn:(NSDate *)fireDate
                          text:(NSString *)alertText
                        action:(NSString *)alertAction
                         sound:(NSString *)soundfileName
                   launchImage:(NSString *)launchImage
                       andInfo:(NSDictionary *)userInfo;

@end

@implementation DDNotificationManagerDescription

@synthesize message;
@synthesize title;
@synthesize isBadge;

- (id)initWithSendingData:(DDMailDataObject *)data withError:(NSError *)error
{
    self = [super init];
    if (self)
    {
        if (!error)
        {
            self.title = @"Success";
            self.message = [NSString stringWithFormat:@"Photo was sent to the mail %@",stringFromDateWithString(data.date, @"HH:mm")];
            self.isBadge = YES;
        }
        else
        {
            self.title = [NSString stringWithFormat:@"Error %@",stringFromDateWithString(data.date, @"HH:mm")];
            self.message = [error localizedDescription];
            self.isBadge = NO;
        }
    }
    return self;
}
@end

static DDNotificationManager *currentNotificationManager = nil;

@implementation DDNotificationManager

@synthesize badgeCount;
@synthesize enable;

+ (DDNotificationManager*)currentManager
{
    return currentNotificationManager;
}

+ (void)initialize
{
    currentNotificationManager = [[DDNotificationManager alloc] init];
}

- (id)init
{
    self = [super init];
    if (self)
    {
        enable = NO;

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(disableSndingNotification)
                                                     name:kDDApplicationResumeNotificationName
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(enableSndingNotification)
                                                     name:kDDApplicationPauseNotificationName
                                                   object:nil];
    }
    return self;
}

- (void)enableSndingNotification
{
    enable = YES;
}

- (void)disableSndingNotification
{
    enable = NO;
}

- (void)scheduleNotificationOn:(NSDate *)fireDate
                          text:(NSString *)alertText
                        action:(NSString *)alertAction
                         sound:(NSString *)soundfileName
                   launchImage:(NSString *)launchImage
                       andInfo:(NSDictionary *)userInfo
{
	UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    localNotification.fireDate = fireDate;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.alertBody = alertText;
    localNotification.alertAction = alertAction;
    localNotification.alertLaunchImage = launchImage;
    localNotification.userInfo = userInfo;
	
	if(soundfileName == nil)
	{
		localNotification.soundName = UILocalNotificationDefaultSoundName;
	}
	else
	{
		localNotification.soundName = soundfileName;
	}
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)setBadgeCount:(int)newBadgeCount
{
    badgeCount = newBadgeCount;
	[UIApplication sharedApplication].applicationIconBadgeNumber = newBadgeCount;
}

- (void)handleReceivedNotification:(UILocalNotification *) thisNotification
{
    
}

- (void)showNotificationWithDescription:(DDNotificationManagerDescription *)messageDescription
{
    [self performSelectorOnMainThread:@selector(showTopViewInfoWithDescription:)
                           withObject:messageDescription
                        waitUntilDone:YES];
}

- (void)showTopViewInfoWithDescription:(DDNotificationManagerDescription *)messageDescription
{
    [self scheduleNotificationOn:[NSDate date]
                            text:messageDescription.message
                          action:@"View"
                           sound:nil
                     launchImage:nil
                         andInfo:nil];
    
    DDTopViewInfoDescription *description = [[DDTopViewInfoDescription alloc] init];
    
    description.message = messageDescription.message;
    description.title = messageDescription.title;
    
    [[DDTopViewInfo defaultTopViewInfo] showMessageWithDescription:description];
}

@end
