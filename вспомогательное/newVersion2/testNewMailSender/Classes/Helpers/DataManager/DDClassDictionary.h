//
//  DDClassDictionary.h
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 4/1/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
	Class dictionary of classes
 */
@interface DDClassDictionary : NSObject

/**
	get object(Class) fo key(Class)
	@param aKey The key(Class) according to which it will be possible to get the value(Class)
	@returns object(Class) corresponding to the key
 */
- (Class)classForKey:(Class)aKey;

/**
	set object(Class) fo key(Class)
	@param classObject stored object(Class)
	@param aKey The key(Class) according to which it will be possible to get the value(Class)
 */
- (void)setClass:(Class)classObject forKey:(Class)aKey;

@end
