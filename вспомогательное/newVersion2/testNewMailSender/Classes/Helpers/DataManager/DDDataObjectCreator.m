//
//  DDDataObjectCreator.m
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 4/1/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDDataObjectCreator.h"
#import "DDDataObject.h"
#import <CoreData/CoreData.h>
#import "DDClassDictionary.h"
#import "DDMailDataObject.h"
#import "DDPhotoFileObject.h"
#import "DDPhotoFile.h"
#import "DDMailData.h"


static DDDataObjectCreator *objectCreator = nil;  

@interface DDDataObjectCreator()

@property(nonatomic,strong)DDClassDictionary *dictionary;

- (DDClassDictionary*)classDictionary;

@end

@implementation DDDataObjectCreator

@synthesize dictionary;

+ (void)initialize
{
    objectCreator = [[DDDataObjectCreator alloc] init];
}

+ (DDDataObjectCreator *)sharedDataObjectCreator
{
    return objectCreator;
}

- (DDClassDictionary *)classDictionary
{
    DDClassDictionary *tempDictionary = [[DDClassDictionary alloc] init];
    [tempDictionary setClass:[DDMailDataObject class] forKey:[DDMailData class]];
    [tempDictionary setClass:[DDPhotoFileObject class] forKey:[DDPhotoFile class]];
    
    return tempDictionary;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.dictionary = [self classDictionary];
    }
    return self;
}

- (id)dataObjectWithManagedObject:(NSManagedObject *)managedObject
{
    Class class = [dictionary classForKey:[managedObject class]];
    id dataObject = [[class alloc] init];

    return dataObject;
}
@end
