//
//  DDFileManager.m
//  mailSender
//
//  Created by Alexander Bondarenko on 08.12.12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDFileManager.h"
#import "DDDatabaseMananger.h"
#import "DDPhotoFileObject.h"

@interface DDFileManager()

+ (NSString *)generateFileName;
+ (NSString *)pathWithName:(NSString *)name;

@end

@implementation DDFileManager

+ (NSString *)generateFileName
{
    CFGregorianDate currentDate = CFAbsoluteTimeGetGregorianDate(CFAbsoluteTimeGetCurrent(), CFTimeZoneCopySystem());
    NSString *fileName = [NSString stringWithFormat:@"image%02ld%02d%02d%02d%03.3f.dt",currentDate.year,currentDate.month, currentDate.hour, currentDate.minute, currentDate.second];
    return fileName;
}

+ (NSString *)pathWithName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    
    return filePath;
}

+ (DDPhotoFileObject *)photoFileObjectWithSavingData:(NSData*)sendingData
{
    if (!sendingData)
    {
        NSLog(@"ERROR: empty data!");
        return nil;
    }
    
    NSString *name = [self generateFileName];
    
    DDPhotoFileObject *photoFileOblect = [[DDPhotoFileObject alloc] init];
    photoFileOblect.fileName = name;
    photoFileOblect.linksCount = 0;
    
    NSString *filePath = [DDFileManager pathWithName:name];
    
    if (![sendingData writeToFile:filePath atomically:YES])
    {
        NSLog(@"ERROR: Unable to write to file!");
    }
    
    return photoFileOblect;
}

+ (NSData*)loadPhotoDataWithPhotoFileObject:(DDPhotoFileObject *)photoFileObject
{
    NSString *filePath = [DDFileManager pathWithName:photoFileObject.fileName];
    
    NSData *tempData = [[NSData alloc] initWithContentsOfFile:filePath];
    
    return tempData;
}

+ (BOOL)removePhotoDataWithPhotoFileObject:(DDPhotoFileObject *)photoFileObject
{
    if (!photoFileObject)
    {
        return NO;
    }
    
    if (photoFileObject.linksCount > 0)
    {
        photoFileObject.linksCount--;
        
        return NO;
    }
    
    NSString *filePath = [self pathWithName:photoFileObject.fileName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];

    __block NSError *error;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        BOOL result = [fileManager removeItemAtPath:filePath error:&error];
        
        if (!result)
        {
            NSLog(@"ERROR: Unable to deleted file!");
        }
    });
    
    return YES;
}

+ (void)addCopyWithPhotoFileObject:(DDPhotoFileObject *)photoFileObject
{
    if (!photoFileObject)
    {
        return;
    }
    
    photoFileObject.linksCount++;
    [[DDDatabaseManager defaultMananger] saveDataObjectWithObject:photoFileObject];
}


@end
