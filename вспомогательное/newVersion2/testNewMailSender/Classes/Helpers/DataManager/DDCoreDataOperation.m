//
//  DDCoreDataOperation.m
//  testDataManager
//
//  Created by Bondarenko Alexander on 1/17/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDCoreDataOperation.h"

@interface NSManagedObjectContext (Additions)

- (BOOL) save;

@end

@implementation NSManagedObjectContext (Additions)

- (BOOL) save
{
    NSError *error = nil;
    BOOL status = YES;
    if ([self hasChanges])
    {
        if (![self save:&error])
        {
            NSLog(@"Unresolved context save error: %@, %@", error, [error userInfo]);
            status = NO;
        }
    }
    return status;
}
@end

@interface DDCoreDataOperation()
{
    void(^block)(NSManagedObjectContext* context);
    
    NSManagedObjectContext *mainManagedObjectContext_;
    NSManagedObjectContext *operationManagedObjectContext_;
    NSMutableArray *changeNotifications_;
}
@property (nonatomic, assign) NSThread *initialThread;
@property (nonatomic, strong) NSManagedObjectContext *mainManagedObjectContext;
@property (nonatomic, strong) NSManagedObjectContext *operationManagedObjectContext;
@property (nonatomic, strong) NSMutableArray *changeNotifications;

- (void) processMainContextChangeNotification:(NSNotification*)notification;
- (void) mergeMainContextChangesIntoOperationContext;
- (void) processOperationContextChangeNotification:(NSNotification*)notification;
- (void) mergeOperationContextChangesIntoMainContext:(NSNotification*)notification;
- (void) doMain;

@end

@implementation DDCoreDataOperation
@synthesize mainManagedObjectContext = mainManagedObjectContext_;
@synthesize operationManagedObjectContext = operationManagedObjectContext_;
@synthesize changeNotifications = changeNotifications_;

- (void) processMainContextChangeNotification:(NSNotification*)notification
{
    @synchronized(self)
    {
        [self.changeNotifications addObject:notification];
    }
}

- (void) mergeMainContextChangesIntoOperationContext
{
    @synchronized(self)
    {
        if ([changeNotifications_ count] > 0)
        {
            for(NSNotification* change in self.changeNotifications)
            {
                [operationManagedObjectContext_ mergeChangesFromContextDidSaveNotification:change];
            }
            
            [operationManagedObjectContext_ save];
            [self.changeNotifications removeAllObjects];
        }
    }
}

- (void) processOperationContextChangeNotification:(NSNotification*)notification
{
    @synchronized(self)
    {
        [self.changeNotifications addObject:notification];
    }
    
    [self performSelector:@selector(mergeOperationContextChangesIntoMainContext:)
                 onThread:self.initialThread
               withObject:notification
            waitUntilDone:NO];
}

- (void) mergeOperationContextChangesIntoMainContext:(NSNotification*)notification
{
    [mainManagedObjectContext_ mergeChangesFromContextDidSaveNotification:notification];
    [mainManagedObjectContext_ save];
}

- (id) initWithMainManagedObjectContext:(NSManagedObjectContext*)context withBlock:(void(^)(NSManagedObjectContext* context))newBlock;
{
    if ((self = [super init]))
    {
        block = newBlock;
        self.initialThread = [NSThread currentThread];
        self.mainManagedObjectContext = context;
        self.operationManagedObjectContext = nil;
        self.changeNotifications = [NSMutableArray arrayWithCapacity:4];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(processMainContextChangeNotification:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:mainManagedObjectContext_];
        
    }
    return self;
}

- (void) main
{
    @autoreleasepool
    {
        @try
        {
            if (![self isCancelled])
            {
                
                self.operationManagedObjectContext = [[NSManagedObjectContext alloc] init];
                [operationManagedObjectContext_ setPersistentStoreCoordinator:[mainManagedObjectContext_ persistentStoreCoordinator]];
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(processOperationContextChangeNotification:)
                                                             name:NSManagedObjectContextDidSaveNotification
                                                           object:operationManagedObjectContext_];
                
                [self doMain];
                
                [self mergeMainContextChangesIntoOperationContext];
            }
        }
        @catch(NSException* exception)
        {
            NSLog(@"Exception: %@, %@", exception.name, exception.reason);
        }
        @catch(...)
        {
            NSLog(@"%@", @"Not NSException-based exception!");
        }
        @finally
        {
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            self.operationManagedObjectContext = nil;
        }
        
    }
}

- (void) doMain
{
    if (block)
    {
        block(self.operationManagedObjectContext);        
    }
}

#pragma mark Memory management

- (void) dealloc
{
    self.initialThread = nil;
    self.mainManagedObjectContext = nil;
    self.changeNotifications = nil;
}

@end
