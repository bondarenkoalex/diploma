//
//  DDDDDatabaseManager.h
//  mailSender
//
//  Created by Alexander Bondarenko on 13.12.12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DDDataObject.h"

#define DATABASE_NAME @"testNewMailSender.sqlite"

/**
	Singleton class for working with CoreData
 */
@interface DDDatabaseManager : NSObject

/**
	gives an instance of DDDatabaseManager;
	@returns DDDatabaseManager instance
 */
+ (DDDatabaseManager *)defaultMananger;

/**
	removes all objects corresponding predicate
	@param predicate predicate characterizing the picks
	@param name the name of the removing entity([DDDataObject dataObjectName])
 */
- (void)removeAllDataObjectWithPredicate:(NSPredicate *)predicate withName:(NSString *)name;

/**
	remove DataObject
	@param dataObject deleted object
 */
- (void)removeDataObjectWithObject:(DDDataObject *)dataObject;

/**
	save DataObject
	@param dataObject stored object
 */
- (void)saveDataObjectWithObject:(DDDataObject *)dataObject;

/**
	Gives into block array of chosen objects according to the  transmitted terms
 	@param block block takes an array of DDDataObject
	@param predicate predicate predicate characterizing the picks
	@param name the name of the entity([DDDataObject dataObjectName])
 */
- (void)pushDataArrayToBlock:(void(^)(NSArray *arrayOfData))block withPredicate:(NSPredicate *)predicate withName:(NSString *)name;
/**
	checks the elements in the database
	@param predicate predicate predicate predicate characterizing the picks
	@param name the name of the entity([DDDataObject dataObjectName])
 */
- (void)checkElementsaExistWithBlock:(void(^)(BOOL isExist))block withPredicate:(NSPredicate *)predicate withName:(NSString *)name;

@end;
