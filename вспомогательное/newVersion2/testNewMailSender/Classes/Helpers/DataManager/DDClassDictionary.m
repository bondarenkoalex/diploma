//
//  DDClassDictionary.m
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 4/1/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDClassDictionary.h"

@interface DDClassDictionary()

@property (nonatomic,strong) NSMutableDictionary *clssDictionary;

@end

@implementation DDClassDictionary

@synthesize clssDictionary;

- (id)init
{
    self = [super init];
    if (self)
    {
        self.clssDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (Class)classForKey:(Class)aKey
{
    NSString *className = [clssDictionary valueForKey:NSStringFromClass (aKey)];
    Class class = NSClassFromString(className);
    
    return class;
}

- (void)setClass:(Class)classObject forKey:(Class)aKey
{
    NSString *classString = NSStringFromClass (classObject);
    NSString *keyString = NSStringFromClass (aKey);
    
    [clssDictionary setValue:classString forKey:keyString];
}

@end
