//
//  DDCoreDataOperation.h
//  testDataManager
//
//  Created by Bondarenko Alexander on 1/17/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

/**
	class operation creates a CoreData context when performing
 */
@interface DDCoreDataOperation : NSOperation

/**
	initializes the object, recommended to call this method during initialization
	@param context CoreData main context in which the merger will be
	@param block block that is called when the operation is starting performed
	@returns return self
 */
- (id)initWithMainManagedObjectContext:(NSManagedObjectContext *)context withBlock:(void(^)(NSManagedObjectContext *context))block;

@end
