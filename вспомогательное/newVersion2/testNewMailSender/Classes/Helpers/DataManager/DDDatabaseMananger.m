//
//  DDDatabaseManager.m
//  mailSender
//
//  Created by Alexander Bondarenko on 13.12.12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDDatabaseMananger.h"
#import "DDCoreDataOperation.h"
#import "DDDataObjectCreator.h"

@interface DDDatabaseManager()

@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, readwrite) dispatch_queue_t  queue;

- (NSArray *)arrayOfCoreDataObjectWithContext:(NSManagedObjectContext *)context withDataName:(NSString *)dataName;
- (NSManagedObject *)coreDataObjectWithObject:(DDDataObject *)dataObject withContext:(NSManagedObjectContext *)context;
- (DDDataObject *)dataWithCoreDataObject:(NSManagedObject *)object;
- (void)removeObject:(NSManagedObject *)object withContext:(NSManagedObjectContext *)context;
- (void)removeDataObjectsWithPredicate:(NSPredicate *)predicate withContext:(NSManagedObjectContext *)context withDataName:(NSString *)dataName;
- (void)removeDataObjectsWithObject:(DDDataObject *)dataObject withContext:(NSManagedObjectContext *)context;
- (void)saveDataObjectWithObject:(DDDataObject *)dataObject withContext:(NSManagedObjectContext *)context;
- (void)saveContext:(NSManagedObjectContext *)context;
- (void)startOperationOnObjectHandlingWithBlock:(void(^)(NSManagedObjectContext *context))block willSaveOnComplition:(BOOL)save;

@end

@implementation DDDatabaseManager

@synthesize managedObjectContext;
@synthesize managedObjectModel;
@synthesize persistentStoreCoordinator;
@synthesize operationQueue;
@synthesize queue;

static DDDatabaseManager *databaseManager;

+ (void)initialize
{
    databaseManager = [[DDDatabaseManager alloc] init]; 
}

+ (DDDatabaseManager *)defaultMananger
{
    return databaseManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.operationQueue = [[NSOperationQueue alloc] init];
        self.queue = dispatch_queue_create("core_data_queue", nil);
    }
    return self;
}

- (void)saveContext:(NSManagedObjectContext *)context
{
    NSError *error = nil;
    [context save:&error];
    
    if (error)
    {
        NSLog(@"eror = %@",error);
    }
}

- (void)startOperationOnObjectHandlingWithBlock:(void(^)(NSManagedObjectContext *context))block willSaveOnComplition:(BOOL)save
{
    DDCoreDataOperation *operation = [[DDCoreDataOperation alloc] initWithMainManagedObjectContext:[self managedObjectContext]
                                 withBlock:^(NSManagedObjectContext *context) {
                                     block(context);
                                     
                                     if (save)
                                     {
                                         [self saveContext:context];
                                     }
                                 }];
    [operationQueue addOperation:operation];
}

- (void)removeAllDataObjectWithPredicate:(NSPredicate *)predicate withName:(NSString *)name
{
    [self startOperationOnObjectHandlingWithBlock:^(NSManagedObjectContext *context) {
        [self removeDataObjectsWithPredicate:predicate withContext:context withDataName:name];   
    } willSaveOnComplition:YES];
}

- (void)removeDataObjectWithObject:(DDDataObject *)dataObject
{
    [self startOperationOnObjectHandlingWithBlock:^(NSManagedObjectContext *context) {
        [self removeDataObjectsWithObject:dataObject withContext:context];
    } willSaveOnComplition:YES];
}

- (void)saveDataObjectWithObject:(DDDataObject *)dataObject
{   
    [self startOperationOnObjectHandlingWithBlock:^(NSManagedObjectContext *context) {
        [self saveDataObjectWithObject:dataObject withContext:context];
    } willSaveOnComplition:YES];
}

- (void)pushDataArrayToBlock:(void(^)(NSArray *arrayOfData))block withPredicate:(NSPredicate *)predicate withName:(NSString *)name
{
    [self startOperationOnObjectHandlingWithBlock:^(NSManagedObjectContext *context) {
        if (block)
        {
            NSArray *arrayOfData = nil;
            
            arrayOfData = [self arrayOfCoreDataObjectWithContext:context withDataName:name];
            
            if (predicate)
            {
                arrayOfData = [arrayOfData filteredArrayUsingPredicate:predicate];
            }
            
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            
            [arrayOfData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [resultArray addObject:[self dataWithCoreDataObject:obj]];
            }];
            
            block(resultArray);
        }
    } willSaveOnComplition:NO];
}

- (void)checkElementsaExistWithBlock:(void(^)(BOOL isExist))block withPredicate:(NSPredicate *)predicate withName:(NSString *)name
{
    [self startOperationOnObjectHandlingWithBlock:^(NSManagedObjectContext *context) {
        if (block)
        {
            NSArray *arrayOfData = [[self arrayOfCoreDataObjectWithContext:context withDataName:name]
                                    filteredArrayUsingPredicate:predicate];
            block(([arrayOfData count] > 0) ? YES : NO);
        }
    } willSaveOnComplition:NO];
}

- (void)removeDataObjectsWithObject:(DDDataObject *)dataObject withContext:(NSManagedObjectContext *)context
{
    NSArray *arrayOfData = [self arrayOfCoreDataObjectWithContext:context withDataName:dataObject.coreDataTypeName];
    [arrayOfData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([dataObject isEqualToCoreDataObject:obj])
        {
            [self removeObject:obj withContext:context];
        }
    }];
}

- (void)saveDataObjectWithObject:(DDDataObject *)dataObject withContext:(NSManagedObjectContext *)context
{
    void (^ __unsafe_unretained __block block)(NSArray *arrayOfDataObjects,NSManagedObject *rootCoreDataObject) = ^(NSArray *arrayOfDataObjects,NSManagedObject *rootCoreDataObject){
        if ([arrayOfDataObjects count] > 0 && arrayOfDataObjects)
        {
            [arrayOfDataObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [obj fillCoreDataObjectWithCoreDataObject:[self coreDataObjectWithObject:obj withContext:context] withRootObject:rootCoreDataObject withBlock:block];
            }];
        }
    };
    
    [dataObject fillCoreDataObjectWithCoreDataObject:[self coreDataObjectWithObject:dataObject withContext:context] withRootObject:nil withBlock:block];
}

- (void)removeDataObjectsWithPredicate:(NSPredicate *)predicate withContext:(NSManagedObjectContext *)context withDataName:(NSString *)dataName
{
    NSArray *arrayOfData = [[self arrayOfCoreDataObjectWithContext:context withDataName:dataName] filteredArrayUsingPredicate:predicate];
    [arrayOfData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self removeObject:obj withContext:context];
    }];
}

- (void)removeObject:(NSManagedObject *)object withContext:(NSManagedObjectContext *)context
{
    DDDataObject *tempObject = [self dataWithCoreDataObject:object];
    
    if (![tempObject canRemoveWithoutPropertiesSaving])
    {
        [self saveDataObjectWithObject:tempObject withContext:context];
    }
    
    [context deleteObject:object];
}

- (NSArray *)arrayOfCoreDataObjectWithContext:(NSManagedObjectContext *)context withDataName:(NSString *)dataName
{
    NSEntityDescription *issueEntity = [NSEntityDescription entityForName:dataName inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:issueEntity];
    NSError *error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    return objects;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *currentManagedObjectContext = self.managedObjectContext;
    if (currentManagedObjectContext != nil)
    {
        if ([currentManagedObjectContext hasChanges] && ![currentManagedObjectContext save:&error])
        {

        }
    }
}

- (NSManagedObject *)coreDataObjectWithObject:(DDDataObject *)dataObject withContext:(NSManagedObjectContext *)context
{
    NSArray *arrayOfData = [self arrayOfCoreDataObjectWithContext:context withDataName:dataObject.coreDataTypeName];
    NSManagedObject *resultObject = nil;
    
    for (int i = 0; i < [arrayOfData count]; i++)
    {
        if ([dataObject isEqualToCoreDataObject:[arrayOfData objectAtIndex:i]])
        {
            resultObject = [arrayOfData objectAtIndex:i];
            break;
        }
    }
    
    if (!resultObject)
    {
        resultObject = [NSEntityDescription insertNewObjectForEntityForName:dataObject.coreDataTypeName inManagedObjectContext:context]; 
    }
    [self saveContext:context];
    
    return resultObject;
}

- (DDDataObject *)dataWithCoreDataObject:(NSManagedObject *)object
{
    DDDataObject *dataObject = [[DDDataObjectCreator sharedDataObjectCreator] dataObjectWithManagedObject:object];
    [dataObject fillSelfWithCoreDataObject:object];
    
    return dataObject;
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
    if (managedObjectContext != nil)
    {
        return managedObjectContext;
    }
     
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        dispatch_sync(queue, ^{
            managedObjectContext = [[NSManagedObjectContext alloc] init];
            [managedObjectContext setPersistentStoreCoordinator:coordinator];
        });
    }
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel != nil)
    {
        return managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator != nil)
    {
        return persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:DATABASE_NAME];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        abort();
    }
    
    return persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
