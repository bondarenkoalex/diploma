//
//  DDDataObjectCreator.h
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 4/1/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DDDataObject;
@class NSManagedObject;

/**
	class-factory to create objects DDDataObject from CoreData objects
 */
@interface DDDataObjectCreator : NSObject

/**
 gives an instance of DDDataObjectCreator;
 @returns DDDataObjectCreator instance
 */
+ (DDDataObjectCreator *)sharedDataObjectCreator;

/**
	creates DDDataObject from CoreData object
	@param managedObject CoreData object from which to create DDDataObject
	@returns return empty DDDataObject
 */
- (id)dataObjectWithManagedObject:(NSManagedObject *)managedObject;

@end
