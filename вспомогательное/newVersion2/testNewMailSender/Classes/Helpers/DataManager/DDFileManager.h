//
//  DDFileManager.h
//  mailSender
//
//  Created by Alexander Bondarenko on 08.12.12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DDPhotoFile;
@class DDPhotoFileObject;

@interface DDFileManager : NSObject

+ (DDPhotoFileObject *)photoFileObjectWithSavingData:(NSData *)sendingData;
+ (NSData *)loadPhotoDataWithPhotoFileObject:(DDPhotoFileObject *)photoFileObject;
+ (BOOL)removePhotoDataWithPhotoFileObject:(DDPhotoFileObject *)photoFileObject;
+ (void)addCopyWithPhotoFileObject:(DDPhotoFileObject *)photoFileObject;
@end
