//
//  DDMailDispatcherNotificationManager.h
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 4/10/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDMailDataObject.h"

typedef enum
{
    DDMailDispatcherNotificationTypeAddMailData = 0,
    DDMailDispatcherNotificationTypeMoveToHistor = 1,
    DDMailDispatcherNotificationTypeRemoveMailData = 3,
    DDMailDispatcherNotificationTypeRemoveAllMailData = 4,
    DDMailDispatcherNotificationTypeInsertMailData = 5,
    DDMailDispatcherNotificationTypeRunSending = 6
}DDMailDispatcherNotificationType;

/**
	Key that allows you to get the type of the notification
 */
static NSString * const kDDMailDispatcherNotificationTypeKey = @"type";

/**
	Key that allows you to get DDMailDataObjec
 */
static NSString * const kDDMailDispatcherMailDataKey = @"mailData";

/**
	Key that allows you to get data status
 */
static NSString * const kDDMailDispatcherMailDataStatusKey = @"status";

/**
	Key that allows you to get Index to insert in queue
 */
static NSString * const kDDMailDispatcherMailDataIndexKey = @"index";

/**
	Class notification manager DDMailDispatcher
 */
@interface DDMailDispatcherNotificationManager : NSObject

/**
	Name of the sent notification
 */
@property(nonatomic,strong) NSString *notificationName;

/**
	Sends a notification about adding data to send
	@param mailDataObject send DDMailDataObject
 */
- (void)sendAddMailDataNotificationWith:(DDMailDataObject *)mailDataObject;

/**
	Sends a notification about moving data to history
	@param mailDataObject DDMailDataObject sent to the history
 */
- (void)sendMoveToHistoryNotificationWithMailData:(DDMailDataObject *)mailDataObject;

/**
	Sends a notification about removing data from the queue
	@param mailDataObject DDMailDataObject remove from queue
 */
- (void)sendRemoveMailDataNotificationWithMailData:(DDMailDataObject* )mailDataObject;

/**
	Sends a notification about removing all data with this status
	@param status data status
 */
- (void)sendRemoveAllMailDataNotificationWithSendingStatus:(DDMailDataObjectSendingStatus)status;

/**
	Sends a notification about insert data into queue
	@param mailDataObject mail data
	@param index index of insert
 */
- (void)sendInsertMailDataNotificationWithMailData:(DDMailDataObject *)mailDataObject toIndex:(int)index;

/**
	Sends a notification about running sending
 */
- (void)sendRunSendingNotification;


@end
