//
//  Operation.h
//  CustomOperations
//
//  Created by Alexander Bondarenko on 12/16/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DDTask;

/**
	Protocol is responsible for the sending status
 */
@protocol DDTaskDelegate <NSObject>

/**
	Call when task was completed
	@param task completed task
 */
- (void)taskCompletedWithTask:(DDTask *)task;

/**
	Call when task was ran retry
 */
- (void)retryCurrentTask;

@end

/**
	Class task for SUQueue
 */
@interface DDTask : NSObject

/**
	Start this task
 */
- (void)start;

/**
	Finish this task
 */
- (void)stop;


/**
	Task run status
 */
@property (nonatomic,readwrite) BOOL isRun;

/**
	Delegate whose methods will be invoked whenever the status
 */
@property (nonatomic,weak)id<DDTaskDelegate>taskDelegate;

@end
