//
//  DDMailDispatcher.h
//  mailSender
//
//  Created by Bondarenko Alexander on 11/29/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDMailSendingTask.h"
#import "DDMailDataObject.h"
#import "DDBackgroundTaskDispatcher.h"
#import "DDMailDispatcherNotificationManager.h"

/**
	Reload notification name
 */
static NSString * const kDDReloadMailOperationNotificationName = @"ReloadMailOperations";

/**
	Maximum count of history objects
 */
static NSInteger const kDDMaxHistoryObjectCount = 30;

/**
	Class for sending mail data and control the sending
 */
@interface DDMailDispatcher : DDBackgroundTaskDispatcher<DDSendingTaskDelegate>

/**
     gives an instance of DDMailDispatcher;
     @returns DDMailDispatcher instance
 */
+ (DDMailDispatcher *)defaultDispatcher;

/**
	Loads the mail data from the DB. Called before using the class and wait a call block.
    @param block that is called after download data from DB
 */
- (void)loadDataWithComplitionBlock:(void(^)())block;

/**
    Call after loadDataWithComplitionBlock to start sending 
 */
- (void)runSending;

/**
	Adds the mail data to send
	@param mailDataObject it is a mail data
 */
- (void)addMailDataObject:(DDMailDataObject *)mailDataObject;

/**
	Remove all mail data in queue with this status
	@param status sending status
 */
- (void)removeAllMailDataObjectWithSendingStatus:(DDMailDataObjectSendingStatus)status;

/**
	Suspend all mail data in queue
 */
- (void)suspendAllSendingMailDataObjects;

/**
	Resume all mail data in queue
 */
- (void)resumeSendingAllMailDataObjects;

/**
	Checks for data with this status
	@param status data status
	@returns return Yes if data exist
 */
- (BOOL)isMailDataObjectsExistWithSendingStatus:(DDMailDataObjectSendingStatus)status;

/**
	Deletes the selected mail data
	@param mailDataObject delete mail data
 */
- (void)removeMailDataObject:(DDMailDataObject *)mailDataObject;

/**
	Inserts the data in the queue at the specified index
	@param mailDataObject it is a mail data
	@param index new index
 */
- (void)insertMailDataObject:(DDMailDataObject *)mailDataObject toIndex:(int)index;

/**
	Gives an array of DDMailDataObject with a specific status
	@param status data status
	@returns array of DDMailDataObject
 */
- (NSArray *)mailDataObjectsWithSendingStatus:(DDMailDataObjectSendingStatus)status;

@end
