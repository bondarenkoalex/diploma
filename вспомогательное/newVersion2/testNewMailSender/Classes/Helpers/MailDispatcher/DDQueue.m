//
//  DDQueue.m
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 2/6/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDQueue.h"

@interface DDQueue()

@property(nonatomic, strong) NSMutableArray *queue;
@property(nonatomic, readwrite) BOOL isStopingQueue;

@end

@implementation DDQueue

@synthesize isStopingQueue;
@synthesize queue;

- (id)init
{
    self = [super init];
    if (self)
    {
        isStopingQueue = NO;
        self.queue = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)resumeQueue
{
    isStopingQueue = NO;
    [self retryCurrentTask];
}

- (void)suspendQueue
{
    isStopingQueue = YES;
    
    if ([queue count] > 0)
    {
        [((DDTask *)[queue objectAtIndex:0]) stop];
    }
}

- (void)reloadWithTaskArray:(NSArray *)taskArray
{
    isStopingQueue = NO;
    [self removeAllTaskWithBlock:nil];
    queue = [NSMutableArray arrayWithArray:taskArray];
    [queue makeObjectsPerformSelector:@selector(setTaskDelegate:) withObject:self];
    
    [self retryCurrentTask];
}

- (void)retryCurrentTask
{
    if ([queue count] <= 0 || isStopingQueue)
    {
        return;
    }
    
    DDTask *currentTask = [queue objectAtIndex:0];
    
    if (!currentTask.isRun)
    {
        [currentTask start];
    }
}

- (void)onCompleteTask:(DDTask *)task
{
    [queue removeObject:task];
    
    if (!isStopingQueue)
    {
        [self retryCurrentTask];
    }
}

- (void)addTaskToTheQueue:(DDTask *)task
{
    task.taskDelegate = self;
    
    [queue addObject:task];
    
    if ([queue count] == 1 && !isStopingQueue)
    {
        [task start];
    }
}

- (void)removeAllTaskWithBlock:(void(^)(DDTask *task))block
{
    while ([queue count] > 0)
    {
        [[queue objectAtIndex:0] stop];
        
        if (block)
        {
            block([queue objectAtIndex:0]);
        }
        
        [queue removeObjectAtIndex:0];
    }
}

- (void)insertTask:(DDTask *)task toIndex:(int)index
{
  if ([queue containsObject:task] && index >= 0 && index < [queue count])
  {
      int oldIndex = [queue indexOfObject:task];
      [queue removeObject:task];
      [queue insertObject:task atIndex:index];
      
      if (oldIndex == 0 || index == 0)
      {
          [queue makeObjectsPerformSelector:@selector(stop)];
          [self retryCurrentTask];
      }
  }
}

- (void)removeTask:(DDTask *)task
{
    if ([task isRun])
    {
        [task stop];
        if ([queue count] > 1)
        {
            DDTask *newCurrentTask = [queue objectAtIndex:1];
            [newCurrentTask start];
        }
    }
    [queue removeObject:task];
}

- (NSArray *)queue
{
    return queue;
}

#pragma mark DDTask delegate

- (void)taskCompletedWithTask:(DDTask *)task
{
    [self removeTask:task];
    [self retryCurrentTask];
}


@end
