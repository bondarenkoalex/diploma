//
//  Operation.m
//  CustomOperations
//
//  Created by Alexander Bondarenko on 12/16/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDTask.h"

@implementation DDTask

@synthesize isRun;
@synthesize taskDelegate;

- (id)init
{
    self = [super init];
    if (self)
    {
        isRun = NO;
    }
    return self;
}

- (void)start
{
    isRun = YES;
}

- (void)stop
{
    isRun = NO;
}

@end
