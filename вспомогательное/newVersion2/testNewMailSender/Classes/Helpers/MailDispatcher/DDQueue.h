//
//  DDQueue.h
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 2/6/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDTask.h"

/**
	Class queue of DDTask
 */
@interface DDQueue : NSObject<DDTaskDelegate>

/**
	Restart queue with array of new tasks
	@param taskArray new tasks
 */
- (void)reloadWithTaskArray:(NSArray *)taskArray;

/**
	Retry current task
 */
- (void)retryCurrentTask;

/**
	Finish selected task
	@param task selected task
 */
- (void)onCompleteTask:(DDTask *)task;

/**
	Added task to the queue
	@param task add task
 */
- (void)addTaskToTheQueue:(DDTask *)task;

/**
	Remove all tasks previously call block for each removing task
    @param block block that will be called before any removing task
 */
- (void)removeAllTaskWithBlock:(void(^)(DDTask *task))block;

/**
	Insert task in the queue with selected index
	@param task inserted task
	@param index index in the queue
 */
- (void)insertTask:(DDTask *)task toIndex:(int)index;

/**
	Remove selected task
	@param task selected task
 */
- (void)removeTask:(DDTask *)task;

/**
	Stop the queue
 */
- (void)suspendQueue;

/**
	Resume the queue
 */
- (void)resumeQueue;

/**
	Get array of tasks
	@returns array of DDTask
 */
- (NSArray *)queue;

@end
