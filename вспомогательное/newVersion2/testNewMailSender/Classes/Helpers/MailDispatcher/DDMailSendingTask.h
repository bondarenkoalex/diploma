//
//  SendingTask.h
//  CustomOperations
//
//  Created by Bondarenko Alexander on 12/17/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDTask.h"
#import "SKPSMTPMessage.h"
#import "DDMessage.h"

@class DDMailDataObject;
@class DDMailSendingTask;

static NSInteger const kDDMaxNumberOfAttempts = 3;

@protocol DDSendingTaskDelegate <NSObject>

@optional

/**
	DDMailSendingTask calls this method when completes sending data
	@param task current task (self)
	@param error returns an error code if an error occurred otherwise nil
 */
- (void)onCompleteTask:(DDMailSendingTask *)task withError:(NSError *)error;

@end

/**
	Class task (operation) for sending a DDMailDataObject
 */
@interface DDMailSendingTask : DDTask<SKPSMTPMessageDelegate>

@property(nonatomic, assign) id<DDSendingTaskDelegate> delegate;
@property(nonatomic, readonly, strong) DDMailDataObject *data;
@property(nonatomic, strong) NSDate *date;

/**
	initializes self with DDMailDataObject
	@param taskData DDMailDataObject
	@returns self
 */
- (id)initWithData:(id)taskData;

@end
