//
//  DDMailDispatcher.m
//  mailSender
//
//  Created by Bondarenko Alexander on 11/29/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDMailDispatcher.h"
#import "DDNotificationManager.h"
#import "DDQueue.h"
#import "DDDatabaseMananger.h"

@interface DDMailDispatcher()

@property(nonatomic,strong) __block NSMutableArray *dataObjects;
@property(nonatomic,strong) DDMailDispatcherNotificationManager *mailDispatcherNotificationManager;
@property(nonatomic,strong) DDQueue *queue;

- (void)changeDataStatusWithData:(DDMailDataObject *)data withError:(NSError *)error;
- (void)filterHistoryDataObjects;
- (void)removeExcessOldDataObjectsWithArray:(NSArray*)dataArray;

@end

@implementation DDMailDispatcher

@synthesize queue;
@synthesize mailDispatcherNotificationManager;
@synthesize dataObjects;

static DDMailDispatcher *defaultDispatcher;

+ (DDMailDispatcher *)defaultDispatcher
{
    return defaultDispatcher;
}

+ (void)initialize
{
    defaultDispatcher = [[DDMailDispatcher alloc] init];
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.dataObjects = [[NSMutableArray alloc] init];
        
        self.mailDispatcherNotificationManager = [[DDMailDispatcherNotificationManager alloc] init];
        mailDispatcherNotificationManager.notificationName = kDDReloadMailOperationNotificationName;
        
        self.queue = [[DDQueue alloc] init];
    }
    return self;
}

#pragma mark publick functions

- (void)loadDataWithComplitionBlock:(void(^)())block
{
   [[DDDatabaseManager defaultMananger] pushDataArrayToBlock:^(NSArray *arrayOfData) {
       dataObjects = [[NSMutableArray alloc] initWithArray:arrayOfData];
       
       if (block)
       {
           block();
       }
       
   } withPredicate:nil withName:[DDMailDataObject dataObjectName]];
}

- (void)addMailDataObject:(DDMailDataObject *)mailDataObject
{
    [self startBackgroundTask];
    
    mailDataObject.status = DDMailDataObjectSendingStatusInQueue;
    
    DDMailSendingTask *task = [[DDMailSendingTask alloc] initWithData:mailDataObject];
    task.delegate = self;
    
    [dataObjects addObject:mailDataObject];
    [queue addTaskToTheQueue:task];
    
    [[DDDatabaseManager defaultMananger] saveDataObjectWithObject:mailDataObject];
    
    [mailDispatcherNotificationManager sendAddMailDataNotificationWith:mailDataObject];
}

- (void)removeAllMailDataObjectWithSendingStatus:(DDMailDataObjectSendingStatus)status
{
    NSArray *tempArray = nil;
    
    if (status == DDMailDataObjectSendingStatusInQueue || status == DDMailDataObjectSendingStatusSending)
    {
       tempArray = [queue.queue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"data.status=%d",status]];
        [tempArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            DDMailSendingTask *task = obj;
            [dataObjects removeObject:task.data];
            [queue removeTask:task];
            [[DDDatabaseManager defaultMananger] removeDataObjectWithObject:task.data];
        }];
    }
    else
    {
        tempArray = [dataObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"status=%d",status]];
        [tempArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [[DDDatabaseManager defaultMananger] removeDataObjectWithObject:obj];  
        }];
        [dataObjects removeObjectsInArray:tempArray];
    }
    
    [mailDispatcherNotificationManager sendRemoveAllMailDataNotificationWithSendingStatus:status];
}

- (void)suspendAllSendingMailDataObjects
{
    [queue suspendQueue];
}

- (void)resumeSendingAllMailDataObjects
{
    [queue resumeQueue];
}

- (BOOL)isMailDataObjectsExistWithSendingStatus:(DDMailDataObjectSendingStatus)status
{
    NSArray *tempArray = [self mailDataObjectsWithSendingStatus:status];
    
    return ([tempArray count] > 0) ? YES : NO;
}

- (void)removeMailDataObject:(DDMailDataObject *)mailDataObject
{
    NSArray *tempArray = [queue.queue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"data.date=%@",mailDataObject.date]];
    
    [tempArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        DDMailSendingTask *task = obj;
        [queue removeTask:task];
        [dataObjects removeObject:task.data];
    }];
    
    [mailDispatcherNotificationManager sendRemoveMailDataNotificationWithMailData:mailDataObject];
}

- (void)insertMailDataObject:(DDMailDataObject *)mailDataObject toIndex:(int)index
{
    NSArray *tempArray = [queue.queue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"data.date=%@",mailDataObject.date]];
    
    if ([tempArray count] > 0)
    {
        [queue insertTask:[tempArray objectAtIndex:0] toIndex:index];   
    }
    
    [mailDispatcherNotificationManager sendInsertMailDataNotificationWithMailData:mailDataObject toIndex:index];
}

- (NSArray*)mailDataObjectsWithSendingStatus:(DDMailDataObjectSendingStatus)status
{
    return [dataObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"status=%d",status]];
}

- (void)runSending
{
    [self startBackgroundTask];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status=%d",
                                                      DDMailDataObjectSendingStatusInQueue];
    
    NSArray *tempDataObjects = [dataObjects filteredArrayUsingPredicate:predicate];
    
    for (int i = 0; i < [tempDataObjects count]; i++)
    {
        DDMailSendingTask *task = [[DDMailSendingTask alloc] initWithData:[tempDataObjects objectAtIndex:i]];
        task.delegate = self;
        [tempArray addObject:task];
    }
    
    [queue reloadWithTaskArray:tempArray];
    
    [mailDispatcherNotificationManager sendRunSendingNotification];
}

#pragma mark suport functions

- (void)removeExcessOldDataObjectsWithArray:(NSArray*)dataArray
{
    NSMutableArray *sortArray = [NSMutableArray arrayWithArray:[dataArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [((DDMailDataObject *)obj1).date compare:((DDMailDataObject *)obj2).date];
    }]];
    
    while ([sortArray count] > kDDMaxHistoryObjectCount)
    {
        DDMailDataObject *data = [sortArray lastObject];
        [dataObjects removeObject:data];
        [[DDDatabaseManager defaultMananger] removeDataObjectWithObject:data];
        [sortArray removeLastObject];
    }
}

- (void)filterHistoryDataObjects
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"status=%d OR status=%d",
                              DDMailDataObjectSendingStatusSuccessful,
                              DDMailDataObjectSendingStatusFail];
    
    NSArray *tempArray = [dataObjects filteredArrayUsingPredicate:predicate];
    
    [self removeExcessOldDataObjectsWithArray:tempArray];
}

- (void)showMesageWithTask:(DDMailSendingTask *)task withError:(NSError *)error
{
    DDNotificationManagerDescription *description = [[DDNotificationManagerDescription alloc] initWithSendingData:task.data withError:error];
    [[DDNotificationManager currentManager] showNotificationWithDescription:description];
}

- (void)changeDataStatusWithData:(DDMailDataObject *)data withError:(NSError *)error
{
    if (error)
    {
        data.status = DDMailDataObjectSendingStatusFail;
    }
    else
    {
       data.status = DDMailDataObjectSendingStatusSuccessful;
    }
    
    [[DDDatabaseManager defaultMananger] saveDataObjectWithObject:data];
    
    [self filterHistoryDataObjects];
    
    [mailDispatcherNotificationManager sendMoveToHistoryNotificationWithMailData:data];
}

#pragma mark DDMailSendingTask delegate

- (void)onCompleteTask:(DDMailSendingTask *)task withError:(NSError *)error
{
    [self showMesageWithTask:task withError:error];
    [self changeDataStatusWithData:task.data withError:error];
}


@end
