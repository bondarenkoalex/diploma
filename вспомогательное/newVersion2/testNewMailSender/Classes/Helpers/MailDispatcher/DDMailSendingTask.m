//
//  SendingTask.m
//  CustomOperations
//
//  Created by Bondarenko Alexander on 12/17/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//


#import "DDMailSendingTask.h"
#import "DDMailDataObject.h"
#import "DDPhotoFileObject.h"


@interface DDMailSendingTask()

@property(nonatomic,readwrite)NSUInteger numberOfAttempts;
@property(nonatomic,strong)DDMessage *smtpMessage;

- (void)performOnCompletionWithError:(NSError*)error;

@end

@implementation DDMailSendingTask

@synthesize data;
@synthesize delegate;
@synthesize smtpMessage;
@synthesize numberOfAttempts;

- (id)initWithData:(id)taskData
{
    self = [super init];
    if (self)
    {
        data = taskData;
        
        numberOfAttempts = 0;
        
        if (!data.date)
        {
            data.date = [NSDate date];
        }
    }
    return self;
}

- (void)start
{
    [super start];
    
    data.status = DDMailDataObjectSendingStatusSending;
    
    self.smtpMessage = [[DDMessage alloc] initWithMailData:data];
    smtpMessage.delegate = self;
    [smtpMessage sendMessage];
}

- (void)stop
{
    if (self.isRun)
    {
        self.isRun = NO;
        [smtpMessage stop]; 
    }
}

- (void)performOnCompletionWithError:(NSError*)error
{
    if ([delegate respondsToSelector:@selector(onCompleteTask:withError:)])
    {
        [self stop];
        [delegate onCompleteTask:self withError:error];
        [self.taskDelegate taskCompletedWithTask:self];
    }
}

- (void)messageSent:(SKPSMTPMessage *)message
{
    [self performOnCompletionWithError:nil];
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    numberOfAttempts++;
    if (numberOfAttempts == kDDMaxNumberOfAttempts)
    {
        [self performOnCompletionWithError:error];
    }
    else
    {
        if ([delegate respondsToSelector:@selector(retryCurrentTask)])
        {
            [self stop];
            [self.taskDelegate retryCurrentTask];
        }
    }
}

- (void)dealloc
{
    self.smtpMessage = nil;
    self.delegate = nil;
}

@end


