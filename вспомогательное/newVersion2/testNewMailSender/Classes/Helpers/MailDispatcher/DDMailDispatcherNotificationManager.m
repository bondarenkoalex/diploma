//
//  DDMailDispatcherNotificationManager.m
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 4/10/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDMailDispatcherNotificationManager.h"

@interface DDMailDispatcherNotificationManager()

- (void)sendNotificationWithDictionary:(NSDictionary*)dictionary;
- (NSMutableDictionary*)dictionaryWithNotificationType:(DDMailDispatcherNotificationType)type;

@end

@implementation DDMailDispatcherNotificationManager
@synthesize notificationName;

- (void)sendAddMailDataNotificationWith:(DDMailDataObject*)mailDataObject
{
    NSMutableDictionary *userInfo = [self dictionaryWithNotificationType:DDMailDispatcherNotificationTypeAddMailData];
    [userInfo setValue:mailDataObject forKey:kDDMailDispatcherMailDataKey];
    
    [self sendNotificationWithDictionary:userInfo];
}

- (void)sendMoveToHistoryNotificationWithMailData:(DDMailDataObject*)mailDataObject
{
    NSMutableDictionary *userInfo = [self dictionaryWithNotificationType:DDMailDispatcherNotificationTypeMoveToHistor];
    [userInfo setValue:mailDataObject forKey:kDDMailDispatcherMailDataKey];

    [self sendNotificationWithDictionary:userInfo];
}

- (void)sendRemoveMailDataNotificationWithMailData:(DDMailDataObject*)mailDataObject
{
    NSMutableDictionary *userInfo = [self dictionaryWithNotificationType:DDMailDispatcherNotificationTypeRemoveMailData];
    [userInfo setValue:mailDataObject forKey:kDDMailDispatcherMailDataKey];
    
    [self sendNotificationWithDictionary:userInfo];
}

- (void)sendRemoveAllMailDataNotificationWithSendingStatus:(DDMailDataObjectSendingStatus)status
{
    NSMutableDictionary *userInfo = [self dictionaryWithNotificationType:DDMailDispatcherNotificationTypeRemoveMailData];
    [userInfo setValue:[NSNumber numberWithInt:status] forKey:kDDMailDispatcherMailDataStatusKey];
    
    [self sendNotificationWithDictionary:userInfo];
}

- (void)sendInsertMailDataNotificationWithMailData:(DDMailDataObject*)mailDataObject toIndex:(int)index
{
    NSMutableDictionary *userInfo = [self dictionaryWithNotificationType:DDMailDispatcherNotificationTypeRemoveMailData];
    [userInfo setValue:mailDataObject forKey:kDDMailDispatcherMailDataKey];
    [userInfo setValue:[NSNumber numberWithInt:index] forKey:kDDMailDispatcherMailDataIndexKey];
    
    [self sendNotificationWithDictionary:userInfo];
}

- (void)sendRunSendingNotification
{
    NSMutableDictionary *userInfo = [self dictionaryWithNotificationType:DDMailDispatcherNotificationTypeRunSending];
    
    [self sendNotificationWithDictionary:userInfo];
}

- (NSMutableDictionary*)dictionaryWithNotificationType:(DDMailDispatcherNotificationType)type
{
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setValue:[NSNumber numberWithInt:type] forKey:kDDMailDispatcherNotificationTypeKey];
    
    return userInfo;
}

- (void)sendNotificationWithDictionary:(NSDictionary*)dictionary
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:self userInfo:dictionary];
    });
}

@end
