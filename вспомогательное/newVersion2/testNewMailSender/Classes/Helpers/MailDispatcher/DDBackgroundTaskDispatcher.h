//
//  DDBackgroundTaskDispatcher.h
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 4/11/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
	Class for controlling the operation in the background
 */
@interface DDBackgroundTaskDispatcher : NSObject

/**
	Allows the application to run in the background
 */
- (void)stopBackgroundTask;

/**
	Prohibits the application to run in the background
 */
- (void)startBackgroundTask;

@end
