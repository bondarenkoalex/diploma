//
//  DDBackgroundTaskDispatcher.m
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 4/11/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDBackgroundTaskDispatcher.h"

@interface DDBackgroundTaskDispatcher()

@property(nonatomic,readwrite) UIBackgroundTaskIdentifier backgroundTask;

@end

@implementation DDBackgroundTaskDispatcher

@synthesize backgroundTask;

- (void)startBackgroundTask
{
    UIApplication *app = [UIApplication sharedApplication];
    
    if (!backgroundTask || backgroundTask == UIBackgroundTaskInvalid)
    {
        backgroundTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:backgroundTask];
            backgroundTask = UIBackgroundTaskInvalid;
        }];
    }
}

- (void)stopBackgroundTask
{
    UIApplication *app = [UIApplication sharedApplication];
    
    if (backgroundTask != UIBackgroundTaskInvalid)
    {
        [app endBackgroundTask:backgroundTask];
        backgroundTask = UIBackgroundTaskInvalid;
    }
}


@end
