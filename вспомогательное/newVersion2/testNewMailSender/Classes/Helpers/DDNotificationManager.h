//
//  DDNotificationManager.h
//  mailSender
//
//  Created by Bondarenko Alexander on 11/29/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDMailDataObject.h"

@interface DDNotificationManagerDescription : NSObject

@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *message;
@property (nonatomic,readwrite) BOOL isBadge;

- (id)initWithSendingData:(DDMailDataObject*)data withError:(NSError*)error;

@end

@interface DDNotificationManager : NSObject

@property(nonatomic,readwrite) int badgeCount;

+ (DDNotificationManager *)currentManager;
- (void)showNotificationWithDescription:(DDNotificationManagerDescription *)messageDescription;
- (void)handleReceivedNotification:(UILocalNotification *)thisNotification;

@end

