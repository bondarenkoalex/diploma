//
//  DDNetworkManager.h
//  mailSender
//
//  Created by Alexander Bondarenko on 12/18/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSInteger const kDDDefaultTimeoutValue = 60;
static NSInteger const kDDCarrierConnectionTimeoutValue = 240;
static NSInteger const kDDWifiConnectionTimeoutValue = 120;

@protocol DDNetworkManagerDelegate <NSObject>

- (void)isConnection:(BOOL)isConnection;

@end

@interface DDNetworkManager : NSObject

@property (nonatomic,assign)id<DDNetworkManagerDelegate>delegate;

- (NSUInteger)timeoutForCurrentConnectionType;
- (void)checkConnectionWithDelegate:(id<DDNetworkManagerDelegate>)newDelegate;
@end
