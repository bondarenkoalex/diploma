//
//  DDNetworkManager.m
//  mailSender
//
//  Created by Alexander Bondarenko on 12/18/12.
//  Copyright (c) 2012 DeadDog. All rights reserved.
//

#import "DDNetworkManager.h"
#import "DDNetworkReachability.h"

@interface DDNetworkManager()

@property (nonatomic, strong) DDNetworkReachability *internetReach;

@end

@implementation DDNetworkManager

@synthesize delegate;
@synthesize internetReach;

- (NSUInteger)timeoutForCurrentConnectionType
{
    DDNetworkReachability *reachibility = [DDNetworkReachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachibility currentReachabilityStatus];
    
    NSUInteger timeoutValue = 0;
    
    if (networkStatus == NotReachable)
    {
        timeoutValue = kDDDefaultTimeoutValue;
    }
    else
    if (networkStatus == ReachableViaWWAN)
    {
        timeoutValue = kDDCarrierConnectionTimeoutValue;;
    }
    else
    if (networkStatus == ReachableViaWiFi)
    {
        timeoutValue = kDDWifiConnectionTimeoutValue;
    }
    
    return timeoutValue;
}

- (void)checkConnectionWithDelegate:(id<DDNetworkManagerDelegate>)newDelegate
{
    self.delegate = newDelegate;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
	
    self.internetReach = [DDNetworkReachability reachabilityForInternetConnection];
	[internetReach startNotifier];
	[self updateInterfaceWithReachability: internetReach];
}

- (void)updateInterfaceWithReachability:(DDNetworkReachability *)curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    [curReach connectionRequired];

    if(netStatus != NotReachable)
    {
        [delegate isConnection:YES];
    }
    else
    {
        [delegate isConnection:NO];
    }
}

- (void)reachabilityChanged:(NSNotification *)note
{
	DDNetworkReachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [DDNetworkReachability class]]);
	[self updateInterfaceWithReachability: curReach];
}

- (void)dealloc
{
    self.delegate = nil;
}
                                     
@end
