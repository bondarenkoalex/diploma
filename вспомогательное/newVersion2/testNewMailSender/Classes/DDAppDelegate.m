//
//  DDAppDelegate.m
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 2/6/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDAppDelegate.h"
#import "DDMailDispatcher.h"

@implementation DDAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[DDMailDispatcher defaultDispatcher] loadDataWithComplitionBlock:^{
        [[DDMailDispatcher defaultDispatcher] runSending];
    }];
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kDDApplicationResumeNotificationName object:nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kDDApplicationPauseNotificationName object:nil];
}

@end
