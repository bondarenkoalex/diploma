//
//  DDViewController.h
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 2/6/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)changeSegmentValue:(id)sender;
- (IBAction)add:(id)sender;
- (IBAction)suspend:(id)sender;
- (IBAction)resume:(id)sender;
- (IBAction)remove:(id)sender;

@end
