//
//  DDViewController.m
//  testNewMailSender
//
//  Created by Bondarenko Alexander on 2/6/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "DDViewController.h"
#import "DDMailDataObject.h"
#import "DDMailDispatcher.h"

@interface DDViewController ()
{
    NSArray *array;
    BOOL isStop;
    DDMailDataObjectSendingStatus status;
}
- (DDMailDataObject*)testDataObject;
@end

@implementation DDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    status = DDMailDataObjectSendingStatusInQueue;
    isStop = NO;
    array = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reload:)
                                                 name:kDDReloadMailOperationNotificationName
                                               object:nil];
}

- (DDMailDataObject*)testDataObject
{
    DDMailDataObject *tempDataObject = [[DDMailDataObject alloc] init];
    
    tempDataObject.date = [NSDate date];
    tempDataObject.username = @"userName";
    tempDataObject.locationTag = @"Kharkov";
    tempDataObject.comment = @"cat";
    tempDataObject.photoObject = [DDFileManager photoFileObjectWithSavingData:UIImagePNGRepresentation([UIImage imageNamed:@"testImage.jpg"])];
    tempDataObject.thumbnail = [UIImage imageNamed:@"testImage.jpg"];
    
    return tempDataObject;
} 

- (void)reload:(NSNotification *)notification
{
    if (notification)
    {
        NSLog(@"reload %@",notification.userInfo);
    }
    
    array = [[DDMailDispatcher defaultDispatcher] mailDataObjectsWithSendingStatus:status];
    [self.tableView reloadData];
}

- (IBAction)add:(id)sender
{
    dispatch_queue_t queue = dispatch_queue_create("addMailDataObject", nil);
    dispatch_async(queue, ^{
        [[DDMailDispatcher defaultDispatcher] addMailDataObject:[self testDataObject]];
    });
}

- (IBAction)suspend:(id)sender
{
    if (isStop)
    {
        return;
    }
    isStop = YES;
    
    dispatch_queue_t queue = dispatch_queue_create("suspendAllSendingMailDataObjects", nil);
    dispatch_async(queue, ^{
        [[DDMailDispatcher defaultDispatcher] suspendAllSendingMailDataObjects];
    });
}

- (IBAction)resume:(id)sender
{
    if (!isStop)
    {
        return;
    }
    isStop = NO;
    
    dispatch_queue_t queue = dispatch_queue_create("resumeSendingAllMailDataObjects", nil);
    dispatch_async(queue, ^{
        [[DDMailDispatcher defaultDispatcher] resumeSendingAllMailDataObjects];
    });
}

- (IBAction)remove:(id)sender
{
    dispatch_queue_t queue = dispatch_queue_create("removeAllMailDataObjectWithSendingStatus", nil);
    dispatch_async(queue, ^{
        [[DDMailDispatcher defaultDispatcher] removeAllMailDataObjectWithSendingStatus:status];
    });
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array count];
}

- (UITableViewCell *)tableView:(UITableView *)newtableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [newtableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] init];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%d)%@",indexPath.row,[[array objectAtIndex:indexPath.row] description]];
    cell.imageView.image = ((DDMailDataObject*)[array objectAtIndex:indexPath.row]).thumbnail;
    
    return cell;
}
- (IBAction)changeSegmentValue:(id)sender
{
    status = ((UISegmentedControl*)sender).selectedSegmentIndex;
    [self reload:nil];
}
@end
